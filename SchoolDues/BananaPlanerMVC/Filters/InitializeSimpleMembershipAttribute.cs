﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using BananaPlanerMVC.Utils;
using SchoolDuesLib;
using SchoolDuesLib.Utils;
using WebMatrix.WebData;
using BananaPlanerMVC.Models;

namespace BananaPlanerMVC.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
/*
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;
*/

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
//            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        internal class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                CreateDb();
            }

            internal static void CreateDb()
            {
                var initDb = new SiteContext("").UserProfiles.FirstOrDefault();
                Database.SetInitializer<UsersContext>(null);

                try
                {
                    using (var context = new UsersContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter) context).ObjectContext.CreateDatabase();
                        }
                    }

//                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);

                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfiles", "Id", "UserName", autoCreateTables: true);

                    if (!Roles.RoleExists(Constants.SystemAdminRoleName))
                        Roles.CreateRole(Constants.SystemAdminRoleName);

                    if (!Roles.RoleExists(Constants.EventAdminRoleName))
                        Roles.CreateRole(Constants.EventAdminRoleName);

                    if (!Roles.RoleExists(Constants.ParentRoleName))
                        Roles.CreateRole(Constants.ParentRoleName);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(
                        "The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588",
                        ex);
                }
            }
        }
    }
}
