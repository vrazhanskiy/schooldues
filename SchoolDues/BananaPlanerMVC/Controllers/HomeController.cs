﻿using System;
using System.Web.Mvc;

namespace BananaPlanerMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult TermsOfService()
        {
            return View();
        }
        public ActionResult Landing()
        {
            return View();
        }

        public ActionResult HowItWorks()
        {
            return View();
        }
    }
}
