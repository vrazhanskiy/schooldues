﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Dwolla.Gateway;
using NLog;
using Newtonsoft.Json;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;

namespace BananaPlanerMVC.Controllers
{
    public class PaymentsController : BaseController
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        //http://stackoverflow.com/questions/8110016/what-is-request-inputstream-and-when-to-use-it
        public ActionResult PayPal()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);  // OK = 200
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">UserId of a user whose dwolla account was used</param>
        /// <param name="response"></param>
        /// <returns></returns>
        public ActionResult Dwolla(DwollaResponse response)
        {
            logger.Info("Dwolla callback");
            logger.Info(JsonConvert.SerializeObject(response));

            Task.Factory.StartNew(() => new PaymenLogDataAccess(GetUserName()).LogDwollaResponse(response));

            // from there find the user account by id
            var inviteDataProvider = new InviteDataProvider(GetUserName());
            var order = inviteDataProvider.GetOrderFromDwollaResponse(response);
            Event ev = inviteDataProvider.GetEventByOrder(order);
            var userPaymentAccount = ev.AcceptablePayments.First();

            string key = userPaymentAccount.Key;
            string secret = userPaymentAccount.Secret;

            var dwollaRequestService = new DwollaServerCheckoutApi(key, secret, true);
            if (dwollaRequestService.VerifyCallbackAuthenticity(response))
            {
                logger.Debug("request verified");
                if (response.Status == DwollaCallbackStatus.Completed)
                {
                    inviteDataProvider.ProcessOrderAndAcceptInvite(order);
                    return new HttpStatusCodeResult(HttpStatusCode.OK);  // OK = 200
                }
                logger.Error("failed to process dwolla callback. Error={0}", response.Error);
                return new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed);
            }
            logger.Error("request failed verification");
            return new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed);
        }
        public ActionResult Stripe()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);  // OK = 200
        }

        public ActionResult Update()
        {
            var userDataAccess = new UserDataAccess(GetUserName());
            var dbUser = userDataAccess.GetUserProfileByUserName(User.Identity.Name);
            return View(dbUser.PaymentAccounts.FirstOrDefault(p => p.Provider == PaymentProvider.Dwolla));
        }

        [HttpPost]
        [Authorize]
        public ActionResult Update(UserPaymentAccount model)
        {
            if (ModelState.IsValid)
            {
                var userDataAccess = new UserDataAccess(GetUserName());
                var dbUser = userDataAccess.GetUserProfileByUserName(User.Identity.Name);
                userDataAccess.AddUpdateDwolla(dbUser, model);

                return RedirectToAction("My", "Account");
            }
            return View(model);
        }
    }
}
