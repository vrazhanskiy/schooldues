﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BananaPlanerMVC.Models;
using BananaPlanerMVC.Utils;
using Dwolla.Gateway;
using NLog;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;
using SchoolDuesLib.Utils;

namespace BananaPlanerMVC.Controllers
{
    [Authorize(Roles = Constants.ParentRoleName)]
    public class InviteController : BaseController
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        //
        // GET: /Invite/
        [AllowAnonymous]
        public ActionResult EmailLinkClicked(string id)
        {
            var byInviteId = new InviteDataProvider(GetUserName()).GetByInviteId(id);
            if (byInviteId == null)
            {
                return View("AppError", new AppErrorModel
                    {
                        Title = "Event not found",
                        Text = "This event was not found. It's possible the event ogranizer decided to delete this event.",
                        Header = "Event not found"
                    });
            }
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Get", new { byInviteId.Id });
            }
            UrlHelper u = new UrlHelper(ControllerContext.RequestContext);
            ViewBag.ReturnUrl = u.Action("Get", "Invite", new { byInviteId.Id });
            ViewBag.Id = id;
            return View("Login");
        }

        public ActionResult Accept(int id, int childId)
        {
            var inviteDataProvider = new InviteDataProvider(GetUserName());

            var model = inviteDataProvider.GetInvite(id, User.Identity.Name);

            if (model.Event.FundsPerPerson == null && model.Event.FundsTotal == null)
            {
                var inviteModel = inviteDataProvider.AcceptInvite(id, childId, User.Identity.Name);
                return RedirectToAction("Get", new { inviteModel.Id });
            }
            if (model.Event.FundsPerPerson != null)
            {
                string callbackUrl = "/Invite/Get/" + model.Id;

                var checkoutRedirectUrl = CreateDwollaUrl(childId, model, inviteDataProvider, callbackUrl);
                return Redirect(checkoutRedirectUrl);
            }
            return null;
        }

        [AllowAnonymous]
        public ActionResult AcceptGuest(int id, string uid)
        {
            var inviteDataProvider = new InviteDataProvider(GetUserName());
            var model = inviteDataProvider.GetByInviteId(uid);

            var child = new ChildDataAccess(GetUserName()).GetDefaultChild(model.Parent.Id);

            if (model.Event.FundsPerPerson == null && model.Event.FundsTotal == null)
            {
                var inviteModel = inviteDataProvider.AcceptInvite(id, child.Id, model.Parent.UserName ); // it's a hack to pass parent username but how else can we make a guest accept?
                return RedirectToAction("GetGuest", new { id = uid });
            }
            if (model.Event.FundsPerPerson != null)
            {
                var callbackUrl = "/Invite/GetGuest/" + uid;
                var checkoutRedirectUrl = CreateDwollaUrl(child.Id, model, inviteDataProvider, callbackUrl);
                return Redirect(checkoutRedirectUrl);
            }
            return null;
        }

        private string CreateDwollaUrl(int childId, Invitation model, InviteDataProvider inviteDataProvider,
                                           string callbackUrl)
        {
            var provider = model.Event.AcceptablePayments.FirstOrDefault(p => p.Provider == PaymentProvider.Dwolla);

            if (provider == null)
            {
                throw new Exception("No Dwolla Account");
            }

            var order = inviteDataProvider.CreateOrGet(model, childId, PaymentProvider.Dwolla);
            if (order.Status == OrderStatus.Complete)
            {
                throw new Exception("Already paid");
            }
            var key = provider.Key;
            var secret = provider.Secret;

            var isTestMode = !Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"]);

            var dwollaRequestService = new DwollaServerCheckoutApi(key, secret, isTestMode);
            var dwollaPurchaseOrder = new DwollaPurchaseOrder
                {
                    DestinationId = provider.AccountId
                };

            dwollaPurchaseOrder.OrderItems.Add(
                new DwollaOrderItem(model.Event.Name, model.Event.FundsPerPerson.Value, 1)
                    {
                        Description = model.Event.Description,
                    });

            var callback = new Uri(GetSiteBaseUrl() + "/Dwolla");
            logger.Debug("Callback {0}", callback);
            logger.Debug("isTestMode {0}", isTestMode);
            var dwollaCheckoutResponse = dwollaRequestService.SendCheckoutRequest(new DwollaCheckoutRequest
                {
                    AllowFundingSources = true,
                    Callback = callback, // to append userid to the end
                    Redirect = new Uri(GetSiteBaseUrl() + callbackUrl),
                    Key = key,
                    Secret = secret,
                    OrderId = order.Id.ToString(),
                    Test = isTestMode,
                    PurchaseOrder = dwollaPurchaseOrder
                });

            if (dwollaCheckoutResponse.Result == DwollaCheckoutResponseResult.Failure)
            {
                logger.Error("Failed to create dwolla request. Message = {0}", dwollaCheckoutResponse.Message);
            }

            var checkoutRedirectUrl = dwollaRequestService.GetCheckoutRedirectUrl(dwollaCheckoutResponse);
            logger.Debug("dwolla url = {0}", checkoutRedirectUrl);
            return checkoutRedirectUrl;
        }

        [AllowAnonymous]
        public ActionResult GetGuest(string id)
        {
            var dataProvider = new InviteDataProvider(GetUserName());
            var model = dataProvider.GetByInviteId(id);
            return View(model);
        }

        public ActionResult Get(int id)
        {
            var model = new InviteDataProvider(GetUserName()).GetInvite(id, User.Identity.Name);
            return View(model);
        }


        public ActionResult AcceptSupply(int id)
        {
            var invite = new InviteDataProvider(GetUserName()).AcceptSupply(id, User.Identity.Name);
            return RedirectToAction("Get", new {invite.Id});
        }

        [AllowAnonymous]
        public ActionResult AcceptSupplyGuest(int id, string uid)
        {
            var dataProvider = new InviteDataProvider(GetUserName());
            var model = dataProvider.GetByInviteId(uid);
            new InviteDataProvider(GetUserName()).AcceptSupply(id, model.Parent.UserName);
            return RedirectToAction("GetGuest", new { id = uid });
        }

        public ActionResult AcceptVolunteer(int id)
        {
            var invite = new InviteDataProvider(GetUserName()).AcceptVolunteer(id, User.Identity.Name);
            return RedirectToAction("Get", new { invite.Id });
        }

        [AllowAnonymous]
        public ActionResult AcceptVolunteerGuest(int id, string uid)
        {
            var dataProvider = new InviteDataProvider(GetUserName());
            var model = dataProvider.GetByInviteId(uid);
            var invite = new InviteDataProvider(GetUserName()).AcceptVolunteer(id, model.Parent.UserName);
            return RedirectToAction("GetGuest", new { id = uid });
        }

        public ActionResult Child(int id)//child id
        {
            var model = new InviteDataProvider(GetUserName()).GetInvitesByChildId(id, User.Identity.Name);
            ViewBag.Student  =  new ChildDataAccess(GetUserName()).GetStudentsById(id, User.Identity.Name);
            return View(model);
        }

    }
}
