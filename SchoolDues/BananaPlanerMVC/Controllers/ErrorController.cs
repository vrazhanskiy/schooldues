﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;

namespace BananaPlanerMVC.Controllers
{
    public class ErrorController : Controller
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        
        public ViewResult Error404()
        {
            logger.Info("Page not found: {0}", Request.Url);
            Response.StatusCode = 200;
            return View("NotFound");
        }
    }
}
