﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using BananaPlanerMVC.Models;
using BananaPlanerMVC.Utils;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;
using SchoolDuesLib.Utils;

namespace BananaPlanerMVC.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        public ActionResult Index()
        {
            if (Roles.IsUserInRole(Constants.ParentRoleName))
            {
                ViewBag.NewUser = !new EventDataProvider(GetUserName()).GetEventsCanAdmin().Any() &&
                                  !new ChildDataAccess(GetUserName()).GetStudentsByParentUsername().Any();

                return View();
            }

            return RedirectToAction("Index", "Event");
        }

        public ActionResult Calendar()
        {
            return View();
        }

        public ActionResult Cal(long start, long end)
        {
            string userName = User.Identity.Name;

            var eventDataProvider = new EventDataProvider(GetUserName());
            var fromDate = DateTime.SpecifyKind(start.FromUnixTimestamp(), DateTimeKind.Unspecified);
            var toDate = DateTime.SpecifyKind(end.FromUnixTimestamp(), DateTimeKind.Unspecified);
            var siteBaseUrl = GetSiteBaseUrl();

            var invites = eventDataProvider.GetEventsForParent(fromDate, toDate,userName);
            var events = eventDataProvider.GetEventsCanAdmin(userName, fromDate, toDate);

            var eventJsons = invites.Select(i => i.ToJson(siteBaseUrl, userName)).ToList();
            eventJsons.AddRange(events.Select(e=>e.ToJson(siteBaseUrl, userName)).ToList());

            return Json(eventJsons, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MyAccount()
        {
            return View();
        }


    }
}