﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;

using System.Web.Security;
using BananaPlanerMVC.Utils;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using NLog;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;
using SchoolDuesLib.Email;
using SchoolDuesLib.Utils;
using WebMatrix.WebData;
using BananaPlanerMVC.Filters;
using BananaPlanerMVC.Models;


namespace BananaPlanerMVC.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : BaseController
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
  //          return RedirectToAction("Login");
            ViewBag.ShowMessage = false;
          return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            
            //if (ModelState.IsValid)
            //{
            //    // Attempt to register the user
            //    try
            //    {
            //        WebSecurity.CreateUserAndAccount(model.UserName, model.Password,
            //            propertyValues: new { Email = model.UserName });
            //        WebSecurity.Login(model.UserName, model.Password);
            //        Roles.AddUserToRole(model.UserName, Constants.ParentRoleName);
            //        return RedirectToAction("Index", "Home");
            //    }
            //    catch (MembershipCreateUserException e)
            //    {
            //        ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
            //    }
            //}

            // If we got this far, something failed, redisplay form
            return View(RegisterUser(model));
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
			log.Debug(string.Format("External Login provider={0}, returnUrl='{1}'", provider, returnUrl));
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            log.Debug("ExternalLoginCallback url = " + returnUrl);

            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                log.Debug("failed auth");
                if (result.Error != null)
                {
                    log.Error("Error", result.Error);
                }
                return RedirectToAction("ExternalLoginFailure");
            }

            string email = GetEmail(result.ExtraData);
            var userDataAccess = new UserDataAccess(GetUserName());
            // try to find a user by email
            UserProfile user = userDataAccess.GetUserProfileByEmailAddress(email);
            bool complete = IsProfileComplete(user);

            // profile complete and can login using the credential provides
            if (complete && OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                if (returnUrl != null)
                {
                    return RedirectToLocal(returnUrl);
                }
                return RedirectToAction("Index", "Dashboard");
            }

            string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);

            // couldn't login using the credentials provides but found a user with that email anyway.
            if (user != null)
            {
                // this might be another external OAuth provider for the same email
                // so let's add it to this user.
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, user.UserName);

                if (!complete)
                {
                    ViewBag.ReturnUrl = returnUrl;
                    return View("ExternalLoginConfirmation", new RegisterExternalLoginModel
                    {
                        ExternalLoginData = loginData,
                        Email = email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Timezone = user.TimezoneId?? 0,
                        Parent = Roles.IsUserInRole(user.UserName, Constants.ParentRoleName)
                    });
                }

                // try to login again
                if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
                {
                    return RedirectToLocal(returnUrl);
                }
                throw new Exception("Login failed for associated account");
            }

            var userName = BuildUserName(result.Provider, result.ProviderUserId);
            userDataAccess.CreateUser(new UserProfile
                {
                    UserName = userName,
                    Email = email,
                });
            OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, userName);
            // User is new, ask for additional information but create a db record right away 
            // and associate the OAuth id with it
                
            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;

            string[] names = GetNames(result.ExtraData);
                
            return View("ExternalLoginConfirmation", new RegisterExternalLoginModel
                {
                    ExternalLoginData = loginData,
                    Email = email,
                    FirstName = names[0],
                    LastName = names[1],
                });
        }

        private bool IsProfileComplete(UserProfile user)
        {
            if (user == null)
            {
                return false;
            }
            return (!string.IsNullOrWhiteSpace(user.FirstName) && !string.IsNullOrWhiteSpace(user.LastName) &&
                    user.Timezone != null);
        }

        private string[] GetNames(IDictionary<string, string> extraData)
        {
            if (extraData.ContainsKey("name"))
            {
                var s = extraData["name"];
                var split = s.Split(' ');
                if (split.Count() >=2)
                {
                    return new[] {split.First(), split.Last()};
                }
                return new[] {s, s};
            }
            return new[] {"", ""};
        }

        private string GetEmail(IDictionary<string, string> extraData)
        {
            if (extraData.ContainsKey("email"))
            {
                return extraData["email"];
            }
            if (extraData.ContainsKey("username"))
            {
                return extraData["username"];
            }
            return null;
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                var userDataAccess = new UserDataAccess(GetUserName());

                // need to find the user created in previous action
                var userName = OAuthWebSecurity.GetUserName(provider, providerUserId);

                // old logic
                var user = userDataAccess.GetUserProfileByUserName(userName);

                /* DO NOT!!!! UPDATE the DB with email from the model as it can be modified by changing a hidden field */

//                var user = userDataAccess.GetUserProfileByOAuth(provider, providerUserId);
                // Insert a new user into the database
                // Check if user already exists
                if (user != null)
                {
                    CopyUserInfoAndApplyRoles(model, user);
                    
                    OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("UserName", "User mapping not found");
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        private static string BuildUserName(string provider, string providerUserId)
        {
            return string.Format("{0}:{1}", provider, providerUserId);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            string path = Request.Path;
            ViewBag.ReturnUrl = returnUrl;
            
                if (path.Contains("Login") || path.Contains("Register"))
                {
                    ViewBag.SiteLoginFlag = false;
                }
            
                else
                {
                    ViewBag.SiteLoginFlag = true;
                }
            
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        public ActionResult My()
        {
            var userProfileByUserName = new UserDataAccess(GetUserName()).GetUserProfileByUserName(User.Identity.Name);

            var schools = userProfileByUserName.UserSchools.ToList();// force load

            var students = new ChildDataAccess(GetUserName()).GetStudentsByParentUsername(User.Identity.Name);

            return View(new FullProfileModel
                {
                    LoginModel = userProfileByUserName,
                    Children = students.ToList()
                });
        }

        public ActionResult AddSchool()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddSchool(School model)
        {
            var userProfileByUserName = new UserDataAccess(GetUserName()).GetUserProfileByUserName(User.Identity.Name);

            new UserDataAccess(GetUserName()).AddSchoolToProfile(userProfileByUserName, model);

            return RedirectToAction("My");
        }

        public ActionResult RemoveSchool(int id)
        {
            var userProfileByUserName = new UserDataAccess(GetUserName()).GetUserProfileByUserName(User.Identity.Name);

            new UserDataAccess(GetUserName()).RemoveSchoolFromProfile(userProfileByUserName, id);
            return RedirectToAction("My");
        }

        public ActionResult Edit()
        {
            var profile = new UserDataAccess(GetUserName()).GetUserProfileByUserName(User.Identity.Name);


            return View(new RegisterExternalLoginModel
                {
                    FirstName = profile.FirstName,
                    LastName = profile.LastName,
                    Email = profile.Email,
                    Parent = Roles.IsUserInRole(Constants.ParentRoleName),
                    Timezone = (profile.Timezone != null)?profile.Timezone.Id: 0
                });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RegisterExternalLoginModel model)
        {
            if (ModelState.IsValid)
            {
                var profile = new UserDataAccess(GetUserName()).GetUserProfileByUserName(User.Identity.Name);
                CopyUserInfoAndApplyRoles(model, profile);
                return RedirectToAction("My");
            }
            return View(model);
        }

        private void CopyUserInfoAndApplyRoles(RegisterExternalLoginModel model, UserProfile profile)
        {
            profile.FirstName = model.FirstName;
            profile.LastName = model.LastName;
            profile.TimezoneId = model.Timezone;

            new UserDataAccess(GetUserName()).UpdateUser(profile);
            var isParentNow = Roles.IsUserInRole(profile.UserName, Constants.ParentRoleName);

            if (model.Parent && !isParentNow)
            {
                Roles.AddUserToRole(profile.UserName, Constants.ParentRoleName);
            }
            if (!model.Parent && isParentNow)
            {
                Roles.RemoveUserFromRole(profile.UserName, Constants.ParentRoleName);
            }
            if (!Roles.IsUserInRole(profile.UserName, Constants.EventAdminRoleName))
            {
                Roles.AddUserToRole(profile.UserName, Constants.EventAdminRoleName);              
            }
        }

        private void CreateAndSignInUser(RegisterModel model, string token)
        {
            Roles.AddUserToRole(model.UserName, Constants.ParentRoleName);

            
            SendEmailController emailController = new SendEmailController();

            emailController.SendConfirmCode(model.UserName, token, System.Web.HttpContext.Current.Request);
            ViewBag.ShowMessage = true;
            FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
        }

        private RegisterModel RegisterUser(RegisterModel model)
        {
            ViewBag.ShowMessage = false;
            if (ModelState.IsValid)
            {
                // Attempt to register the user

                //MembershipUser user = null;
                //MembershipCreateStatus createStatus; not supported anymore
              
                    model.UserName = model.UserName.Trim();
                  //  user = Membership.CreateUser(model.UserName, model.Password, model.UserName, null, null, true, null, out createStatus);
                    string token = WebSecurity.CreateUserAndAccount(model.UserName, model.Password,
                        propertyValues: new { Email = model.UserName }, requireConfirmationToken: true);
                    
              //  if (createStatus == MembershipCreateStatus.Success)
                    if(token != null)
                {
                    CreateAndSignInUser(model, token);
                }
                else
                {
                    ModelState.AddModelError((string)"", (string)ErrorCodeToString(MembershipCreateStatus.UserRejected));
                }
            }

            return model;
        }

        public ActionResult Verify(string id /*confirmcode*/)
        {
            string confirmcode = id; // it's easier than creating a whole new route
            ViewBag.IsVerified = false;
           
            if (string.IsNullOrEmpty(confirmcode) == false)
            {
                
                if (WebSecurity.ConfirmAccount(confirmcode))
                {

                    FormsAuthentication.SetAuthCookie(GetUserName(), false);
                    ViewBag.UserId = confirmcode;
                    ViewBag.IsVerified = true;
                }
            }


            return View();
        }

      
    }
}
