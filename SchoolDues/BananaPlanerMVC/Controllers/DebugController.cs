﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BananaPlanerMVC.Utils;
using Microsoft.Web.WebPages.OAuth;
using SchoolDuesLib;
using SchoolDuesLib.Email;
using SchoolDuesLib.Utils;
using WebMatrix.WebData;

namespace BananaPlanerMVC.Controllers
{
    [Authorize(Roles = Constants.SystemAdminRoleName)]
    public class DebugController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            return View();
        }

        public ActionResult Test()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Error()
        {
            throw new Exception("Testing error");
        }

        public ActionResult LoginAs(int id)
        {
            WebSecurity.Logout();

            var user = new SiteContext().UserProfiles.First(u => u.Id == id);
            var account = OAuthWebSecurity.GetAccountsFromUserName(user.UserName).First();

            OAuthWebSecurity.Login(account.Provider, account.ProviderUserId, createPersistentCookie: false);
            return RedirectToAction("Index", "Dashboard");
        }
    }
}
