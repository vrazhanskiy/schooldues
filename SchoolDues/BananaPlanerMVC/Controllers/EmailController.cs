﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Configuration;
using System.Web.Razor;
using System.Web.Mvc;
using System.Web.Mvc.Razor;
using BananaPlanerMVC.Models;
using SchoolDuesLib.Email;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;


namespace BananaPlanerMVC.Controllers
{
    public class EmailController : BaseController
    {
        private static readonly string fromEmail = ConfigurationManager.AppSettings["emailFrom"];
        private const string ConfirmEmailSubject = "Confirm your email address";

        public ActionResult Send(SendEmailModel model)
        {
            var eventDataProvider = new EventDataProvider(User.Identity.Name);
            
            var controller = new SendEmailController();
            bool result = true;
            switch (model.Action)
            {
                case SendMailAction.AcceptedParticipants:
                    Event ev = eventDataProvider.GetEventAsAdmin(model.Id);
                    result = controller.ProcessEmailToParticipatns(ev, ev.Invitations.Where(i=>i.Status == InvitationStatus.Accepted).ToList(), model.Subject,
                                                                   model.Body, GetSiteBaseUrl());
                    break;
                case SendMailAction.AllParticipants:
                    ev = eventDataProvider.GetEventAsAdmin(model.Id);
                    result = controller.ProcessEmailToParticipatns(ev, ev.Invitations.ToList(), model.Subject,
                                               model.Body, GetSiteBaseUrl());
                    break;
                case SendMailAction.NotAcceptedParticipants:
                    ev = eventDataProvider.GetEventAsAdmin(model.Id);
                    result = controller.ProcessEmailToParticipatns(ev, ev.Invitations.Where(i => i.Status != InvitationStatus.Accepted).ToList(), model.Subject,
                                               model.Body,GetSiteBaseUrl());
                    break;
                case SendMailAction.Organizer:
                    var invite = new InviteDataProvider(User.Identity.Name).GetInvite(model.Id);
                    result = controller.ProcessEmailToOrganizer(invite, model.Subject, model.Body, GetSiteBaseUrl());
                    break;
            }

            return Json(result, JsonRequestBehavior.DenyGet);
        }

       
    }

}
