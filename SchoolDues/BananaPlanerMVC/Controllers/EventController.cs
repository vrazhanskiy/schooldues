﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;

namespace BananaPlanerMVC.Controllers
{
    [Authorize]
    public class EventController : BaseController
    {
        public ActionResult Get(int id)
        {
            var model = new EventDataProvider(GetUserName()).GetEventAsAdmin(id, User.Identity.Name);
            return View(model);
        }

        public ActionResult Index()
        {
            var eventDataProvider = new EventDataProvider(GetUserName());
            return View(eventDataProvider.GetEventsCanAdmin(fromDate:DateTime.Today).ToList());
        }

        public ActionResult Past()
        {
            return View(new EventDataProvider(GetUserName()).GetEventsCanAdmin(fromDate:new DateTime(1970,1,1), toDate:DateTime.Today).ToList());
        }

        private void PrepareViewModelForAddUpdateScreen(Event model)
        {
/*
            List<School> userSchools = new SchoolDataProvider(GetUserName()).GetUserSchools(User.Identity.Name);
            ViewBag.Schools = userSchools;
*/
            if (model.School != null)
            {
                model.SchoolId = model.School.Id;
                model.SchoolRelated = true;
            }
            else
            {
                model.SchoolRelated = false;
            }
            if (model.StartTime != null)
            {
                model.Date = model.StartTime.Value;
            }
            if (model.FundsTotal != null)
            {
                model.FundSelection = 2;
            } 
            else if (model.FundsPerPerson != null)
            {
                model.FundSelection = 1;
            }
            else
            {
                model.FundSelection = 0;
            }
        }

        public ActionResult Create()
        {
            var date = DateTime.Today.AddDays(1);
            var model = new Event
            {
                Date = date,
                StartTime = new DateTime(date.Year, date.Month, date.Day, 11, 0, 0),
                EndTime = new DateTime(date.Year, date.Month, date.Day, 13, 0, 0),
            };

            PrepareViewModelForAddUpdateScreen(model);

//            model.SchoolRelated = ((IList<School>) ViewBag.Schools).Any();
            return View("AddUpdate", model);
        }

        [HttpPost]
        public ActionResult Create(Event model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.IsDraft = true;

                UpdateModelFromViewModel(model);
                model = new EventDataProvider(GetUserName()).CreateEvent(model, User.Identity.Name);

                if (model.NeedVolunteers == true)
                {
                    return RedirectToAction("Volunteers", new { id = model.Id });
                }
                if (model.NeedSupplies == true)
                {
                    return RedirectToAction("Supplies", new { id = model.Id });
                }
                return RedirectToAction("Invite", new {id = model.Id});
            }

            PrepareViewModelForAddUpdateScreen(model);
            return View("AddUpdate", model);
        }

        public ActionResult Edit(int id)
        {
            var model = new EventDataProvider(GetUserName()).GetEventAsAdmin(id);
            PrepareViewModelForAddUpdateScreen(model);
            return View("AddUpdate", model);
        }

        [HttpPost]
        public ActionResult Edit(Event model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedOn = DateTime.Now;

                UpdateModelFromViewModel(model);
                model = new EventDataProvider(GetUserName()).UpdateEvent(model, User.Identity.Name);

                if (model.NeedVolunteers == true)
                {
                    return RedirectToAction("Volunteers", new { id = model.Id });
                }
                if (model.NeedSupplies == true)
                {
                    return RedirectToAction("Supplies", new { id = model.Id });
                }
                return RedirectToAction("Invite", new { id = model.Id });
            }

            PrepareViewModelForAddUpdateScreen(model);
            return View("AddUpdate", model);
        }

        private static void UpdateModelFromViewModel(Event model)
        {
            Debug.Assert(model.StartTime != null);
            Debug.Assert(model.EndTime != null);

            if (!model.SchoolRelated)
            {
                model.School = null;
                model.SchoolId = null;
            }

            model.StartTime = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day,
                                           model.StartTime.Value.Hour, model.StartTime.Value.Minute, 0, model.StartTime.Value.Kind);

            model.EndTime = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day,
                                         model.EndTime.Value.Hour, model.EndTime.Value.Minute, 0, model.StartTime.Value.Kind);


        }

        public ActionResult Volunteers(int id)
        {
            var model = new EventDataProvider(GetUserName()).GetEventAsAdmin(id, User.Identity.Name);
/*
            PrepareViewModelForAddUpdateScreen(model);
            model.VolunteerSpots.Add(new VolunteerSpot {Comments = "Test", NumberOfVolunteers = 2, StartTime = DateTime.Now, EndTime = DateTime.Now.AddHours(1)});
            model.VolunteerSpots.Add(new VolunteerSpot { Comments = "Test2", NumberOfVolunteers = 20, StartTime = DateTime.Now, EndTime = DateTime.Now.AddHours(1) });
*/

            if (model.NeedVolunteers == true && model.VolunteerSpots.Count == 0)
            {
                model.VolunteerSpots.Add(CreateEmptyVolunteerSpot(model));
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Volunteers(Event model, string button)
        {
            if (button != "Add Volunteer Spot")
            {
                model = new EventDataProvider(GetUserName()).UpdateEventVolunteers(model, User.Identity.Name);
                return RedirectToAction(GetNextEventScreen("Volunteers", model), new { id = model.Id });
            }

            // read the event from DB so we can use start/end time to create new volunteer spot
            var curModel = new EventDataProvider(GetUserName()).GetEventAsAdmin(model.Id, User.Identity.Name);
            model.VolunteerSpots.Add(CreateEmptyVolunteerSpot(curModel));
            
            model = new EventDataProvider(GetUserName()).UpdateEventVolunteers(model, User.Identity.Name);
            return RedirectToAction("Volunteers", new { id = model.Id });
        }

        private static VolunteerSpot CreateEmptyVolunteerSpot(Event model)
        {
            var volSpot = new VolunteerSpot
                {
                    NumberOfVolunteers = 1
                };
            if (model.StartTime != null)
            {
                volSpot.StartTime = model.StartTime.Value;
            }
            if (model.EndTime != null)
            {
                volSpot.EndTime = model.EndTime.Value;
            }
            return volSpot;
        }

        public ActionResult Supplies(int id)
        {
            var model = new EventDataProvider(GetUserName()).GetEventAsAdmin(id, User.Identity.Name);
            if (model.Supplies.Any())
            {
                ViewBag.SuppliesString = string.Join("\r\n", model.Supplies.Select(s => s.Description));
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Supplies(Event model, string supplies, string nextPage)
        {
            new EventDataProvider(GetUserName()).UpdateSupplies(model, Split(supplies), User.Identity.Name);
            return RedirectToAction(GetNextEventScreen("Supplies", model), new { id = model.Id});
        }

        private string GetNextEventScreen(string currentScreenName, Event ev)
        {
            var hiddenField = HttpContext.Request["nextPage"];
            if (!string.IsNullOrWhiteSpace(hiddenField))
            {
                return hiddenField;
            }
            switch (currentScreenName)
            {
                case "Supplies":
                    return "Invite";
                case "Volunteers":
                    return ev.NeedSupplies == true ? "Supplies" : "Invite";
                case "Invite":
                    return "Get";

            }
            return null;
        }


        public ActionResult Invite(int id)
        {
            var model = new EventDataProvider(GetUserName()).GetEventAsAdmin(id, User.Identity.Name);
            if (model.Invitations.Any())
            {
                ViewBag.Emails = string.Join("\r\n", model.Invitations.Select(s => s.Parent.Email));
            }
            var otherAdmins = model.Admins.Where(a => a.UserName != User.Identity.Name).ToList();
            if (otherAdmins.Any())
            {
                ViewBag.Admins = string.Join("\r\n", otherAdmins.Select(a => a.Email));
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Invite(Event model, string emails, string admins)
        {
            new EventDataProvider(GetUserName()).UpdateInvites(model, Split(emails),  User.Identity.Name, GetSiteBaseUrl());
            new EventDataProvider(GetUserName()).UpdateAdmins(model, Split(admins), User.Identity.Name);
            return RedirectToAction(GetNextEventScreen("Invite", model), new { id = model.Id });
        }

        private string[] Split(string fullStr)
        {
            return fullStr.Split('\r', '\n').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s=>s.Trim()).ToArray();
        }

        public ActionResult Publish(int id)
        {
            var eventDataProvider = new EventDataProvider(GetUserName());
            var ev = eventDataProvider.Publish(id, User.Identity.Name, GetSiteBaseUrl());
            var selfInvite = ev.Invitations.FirstOrDefault(i => i.Parent.UserName == GetUserName());
            if(selfInvite != null)
            {
                return RedirectToAction("Get", "Invite", new {selfInvite.Id});
            }

            return RedirectToAction("Get", new {id});
        }

    }
}
