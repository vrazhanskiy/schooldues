﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BananaPlanerMVC.Models;
using BananaPlanerMVC.Utils;
using NLog;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;
using SchoolDuesLib.Utils;

namespace BananaPlanerMVC.Controllers
{
    [Authorize(Roles = Constants.ParentRoleName)]
    public class StudentController : BaseController
    {
        private readonly IList<SelectListItem> gradesSelectListItems;
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public StudentController()
        {
            gradesSelectListItems = new List<SelectListItem>
                {
                    new SelectListItem {Text = "K", Value = "K"}
                };
            for (int i = 1; i <=12; i++)
            {
                gradesSelectListItems.Add(new SelectListItem {Value = i.ToString(), Text = i.ToString()});
            }
        }

        public ActionResult Update(int? id)
        {
            ViewBag.Grades = gradesSelectListItems;
            if (id != null)
            {
                var student = new ChildDataAccess(GetUserName()).GetStudentsById(id.Value, User.Identity.Name);   
                return View(student);
            }
            return View();
        }
/*
        public ActionResult Edit(int Id)
        {
            ViewBag.Grades = gradesSelectListItems;
            var student = new ChildDataAccess(GetUserName()).GetStudentsById(Id, User.Identity.Name);   
            return View("Add", student);
        }
*/
        public ActionResult Add()
        {
            ViewBag.Grades = gradesSelectListItems;
            return RedirectToAction("Update");
        }
        /*

                public ActionResult Add()
                {
                    ViewBag.Grades = gradesSelectListItems;
                    return View();
                }
        */

        [HttpPost]
        public ActionResult Update(Student model)
        {
            if (ModelState.IsValid)
            {
                // todo validate that user has access to this student
                new ChildDataAccess(GetUserName()).CreateUpdateChild(model, User.Identity.Name);
                return RedirectToAction("My", "Account");
            }

            ViewBag.Grades = gradesSelectListItems;
            return View(model);
        }

        public ActionResult AutocompleteSchoolsObj(string term, string city, string state)
        {
            if (!string.IsNullOrWhiteSpace(state) && !string.IsNullOrWhiteSpace(city))
            {
                log.Debug("Making a request for city {0} state {1} term {2}", city, state, term);
                var schoolsByTownZip = new SchoolDataProvider(GetUserName()).GetSchoolsByTownZip(city, state);
                var list = schoolsByTownZip.Where(s => s.Name.ToUpper().Contains(term.ToUpper())).Select(s =>new {s.Name, s.Id}).Distinct().ToList();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// find classroom by teacher name and school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public ActionResult AutocompleteClassRoom(string schoolId, string term)
        {
/*
            if (!string.IsNullOrWhiteSpace(state) && !string.IsNullOrWhiteSpace(city))
            {
                var schoolsByTownZip = new SchoolDataProvider().GetSchoolsByTownZip(city, state);
                var list = schoolsByTownZip.Where(s => s.Name.ToUpper().Contains(term.ToUpper())).Select(s =>new {s.Name, s.Id}).Distinct().ToList();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
*/
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}
