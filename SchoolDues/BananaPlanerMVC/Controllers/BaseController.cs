using System.Web.Mvc;

namespace BananaPlanerMVC.Controllers
{
    public class BaseController:Controller
    {
        protected string GetSiteBaseUrl()
        {
            return string.Format("{0}://{1}{2}",
                                 Request.Url.Scheme,
                                 Request.Headers["host"],
                                 Request.ApplicationPath.TrimEnd('/'));
        }

        protected string GetUserName()
        {
            return User != null ? User.Identity.Name : null;
        }
    }
}