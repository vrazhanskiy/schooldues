namespace BananaPlanerMVC.Models
{
    public class EventJson
    {
// ReSharper disable InconsistentNaming
        public int id { get; set; }
        public string title { get; set; }
        public bool allDay { get; set; }
        public long start { get; set; }
        public long end{ get; set; }
        public bool ignoreTimezone { get; set; }
        public string url { get; set; }
        public string className { get; set; }
        public bool editable{ get; set; }
        public string color  { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public string textColor { get; set; }
// ReSharper restore InconsistentNaming
    }
}