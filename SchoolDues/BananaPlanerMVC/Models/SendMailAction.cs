﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BananaPlanerMVC.Models
{
    public enum SendMailAction
    {
        AllParticipants,
        AcceptedParticipants,
        NotAcceptedParticipants,
        Organizer
    }
}