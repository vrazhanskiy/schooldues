namespace BananaPlanerMVC.Models
{
    public class SendEmailModel
    {
        public SendMailAction Action { get; set; }
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}