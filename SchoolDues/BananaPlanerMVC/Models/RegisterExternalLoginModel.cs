using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SchoolDuesLib.Models;

namespace BananaPlanerMVC.Models
{
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Also a parent?")]
        public Boolean Parent { get; set; }

        [Required]
        [Display(Name = "Your timezone")]
        [Range(1,200, ErrorMessage = "Please select a timezone")]
        public int Timezone{ get; set; }

        public String Email { get; set; }
        public string ExternalLoginData { get; set; }
    }

    public class FullProfileModel
    {
        public UserProfile LoginModel { get; set; }
        public IList<Student> Children { get; set; }
    }
}