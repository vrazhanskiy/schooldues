﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BananaPlanerMVC.Models
{
    public class AppErrorModel
    {
        public string Title { get; set; }
        public string Header{ get; set; }
        public string Text { get; set; }
    }
}