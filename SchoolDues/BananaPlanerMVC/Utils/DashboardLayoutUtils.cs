﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace BananaPlanerMVC.Utils
{
    public static class LinkExtensions
    {
        public static MvcHtmlString NavLink(
            this HtmlHelper htmlHelper,
            string linkText,
            string action,
            string controller,
            RouteValueDictionary routValues = null,
            IDictionary<String, Object> htmlAttributes = null,
            string[] liClass = null
        )
        {
            var li = new TagBuilder("li");
            if (liClass != null)
            {
                foreach (var cl in liClass)
                {
                    li.AddCssClass(cl);
                }
            }
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            var currentPage = action == currentAction && controller == currentController;

            // compare Ids
            if (htmlHelper.ViewContext.RouteData.Values.ContainsKey("id") && routValues != null && routValues.ContainsKey("id"))
            {
                currentPage &= (htmlHelper.ViewContext.RouteData.Values["id"].ToString() == routValues["id"].ToString());
            }

            if (currentPage)
            {
                li.AddCssClass("active");
            }
            else
            {
            }
            li.InnerHtml = htmlHelper.ActionLink(linkText, action, controller, routValues, htmlAttributes).ToString();
            return MvcHtmlString.Create(li.ToString());
        }
    }
}