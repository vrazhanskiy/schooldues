using System;

namespace BananaPlanerMVC.Utils
{
    public static class JsonUtils
    {
        private static readonly DateTime unixStartOfTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static DateTime FromUnixTimestamp(this long timestamp)
        {
            return unixStartOfTime.AddSeconds(timestamp);
        }

        public static long ToUnixTimestamp(this DateTime dt)
        {
            return (dt.Ticks - unixStartOfTime.Ticks) / 10000000;
        }
    }
}