﻿using System.Text;
using BananaPlanerMVC.Models;
using NLog;
using SchoolDuesLib.Models;
using SchoolDuesLib.Providers;
using SchoolDuesLib.Utils;

namespace BananaPlanerMVC.Utils
{
    public static class FormatUtils
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        
        public static string FormatParticipantName(this Invitation invite, bool includeEmail)
        {
            if (invite.Student.FirstName == ChildDataAccess.DefaultStudentFirstName && invite.Student.LastName == ChildDataAccess.DefaultStudentLastName)
            {
                return invite.Parent.Format(includeEmail);
            }
            return invite.Student.Format(includeEmail);
        }


        public static string Format(this UserProfile profile, bool includeEmail)
        {
            StringBuilder sb = new StringBuilder();
            bool hasName = false;
            if (!string.IsNullOrWhiteSpace(profile.FirstName))
            {
                sb.Append(profile.FirstName);
                sb.Append(" ");
                hasName = true;
            }
            if (!string.IsNullOrWhiteSpace(profile.LastName))
            {
                sb.Append(profile.LastName);
                sb.Append(" ");
                hasName = true;
            }
            if (!hasName)
            {
                return profile.Email;
            }
            if (includeEmail)
            {
                return string.Format("{0} <{1}>", sb.ToString(), profile.Email);
            }
            return sb.ToString();
        }

        public static string Format(this Invitation invite)
        {
            return invite.Event.Format();
        }

        public static string Format(this Event ev)
        {
            return string.Format("{0} ({1})", ev.Name,
                                 ev.StartTime.Value.ToString("MM/dd/yy hh:mm tt"));
        }

        public static EventJson ToJson(this Invitation invitation, string baseUrl, string userName)
        {
            var eventJson = new EventJson
                {
                    id = invitation.Id,
                    start = invitation.Event.StartTime.Value.ToUserUtcTime(userName).ToUnixTimestamp(),
                    end = invitation.Event.EndTime.Value.ToUserUtcTime(userName).ToUnixTimestamp(),
                    ignoreTimezone = false,
                    editable = false,
                    url = string.Format("{0}/invite/get/{1}", baseUrl, invitation.Id),
                    backgroundColor = "#eeffee",
                    textColor = "#000000"
                };
            eventJson.title = invitation.Student != null
                                  ? string.Format("{0}: {1}", invitation.Student.FirstName, invitation.Event.Name)
                                  : invitation.Event.Name;
            return eventJson;
        }

        public static EventJson ToJson(this Event ev, string baseUrl, string userName)
        {
            return new EventJson
                {
                    id = ev.Id,
                    title = "Admin: " + ev.Name,
                    start = ev.StartTime.Value.ToUserUtcTime(userName).ToUnixTimestamp(),
                    end = ev.EndTime.Value.ToUserUtcTime(userName).ToUnixTimestamp(),
                    ignoreTimezone = false,
                    editable = false,
                    url =string.Format("{0}/event/get/{1}",baseUrl, ev.Id),
                    backgroundColor = "#00aaaa",
                    textColor = "#000000"
                };
        }
    }
}