﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using BananaPlanerMVC.Models;

namespace BananaPlanerMVC
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            OAuthWebSecurity.RegisterFacebookClient(
                ConfigurationManager.AppSettings["facebookAppId"],
                ConfigurationManager.AppSettings["facebookSecret"]);


            // localhost
//            OAuthWebSecurity.RegisterFacebookClient("198765063604865", "d61bf194522c93e19a589c163dfd46a9");
            OAuthWebSecurity.RegisterGoogleClient();
        }
    }
}
