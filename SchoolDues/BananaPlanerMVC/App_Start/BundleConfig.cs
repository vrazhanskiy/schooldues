﻿using System.Web;
using System.Web.Optimization;

namespace BananaPlanerMVC
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.js", "~/Scripts/knockout.mapping-latest.js"/*, "~/Scripts/perpetuum.knockout.js"*/));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js")); 

            bundles.Add(new ScriptBundle("~/bundles/bootstraper-datetime").Include(
                        "~/Scripts/bootstrap-datepicker.js","~/Scripts/bootstrap-timepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/venera").Include(
                        "~/Scripts/venera/*.js","~/Scripts/qtip2/jquery.qtip.js"));
            bundles.Add(new ScriptBundle("~/bundles/bp").Include(
                        "~/Scripts/bp.js"));

            bundles.Add(new ScriptBundle("~/bundles/fullcalendar").Include("~/Scripts/fullcalendar.js"));

            // styles
            bundles.Add(new StyleBundle("~/Content/fullcalendar").Include("~/Content/fullcalendar.css"/*, "~/Content/fullcalendar.print.css"*/));

            bundles.Add(new StyleBundle("~/Content/venera-css")
                .Include("~/Content/qtip2/jquery.qtip.css","~/Content/themes/venera/theme_venera.css","~/Content/venere-extend.css",
                "~/Content/qtip2/jquery.qtip.banana.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css")); // not used

            bundles.Add(new StyleBundle("~/Content/bootstraper-datetime")
                .Include("~/Content/bootstrap-timepicker.css","~/Content/datepicker.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery-ui.css",
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/zocial-css").Include("~/Content/zocial/zocial.css"));
        }
    }
}