﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BananaPlanerMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Dwolla",
                url: "Dwolla",
                defaults: new { controller = "Payments", action = "Dwolla", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Stripe",
                url: "Stripe",
                defaults: new { controller = "Payments", action = "Stripe", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "PayPal",
                url: "PayPal",
                defaults: new { controller = "Payments", action = "PayPal", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Invite",
                url: "Invite/{id}",
                defaults: new { controller = "Invite", action = "EmailLinkClicked", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "InviteController",
                url: "Invite/{action}/{id}",
                defaults: new { controller = "Invite", action = "GetP", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}