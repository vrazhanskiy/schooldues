﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using BananaPlanerMVC.Controllers;

namespace BananaPlanerMVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            if (ConfigurationManager.AppSettings["LandingPage"] != null)
            {
                filters.Add(new HomeOnly());
            }
        }
    }

    public class HomeOnly : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((!(filterContext.Controller is HomeController) ||
                 (filterContext.ActionDescriptor.ActionName != "Landing")) &&
                !(filterContext.Controller is ErrorController))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {{"controller", "Home"}, {"action", "Landing"}});
            }
        }
    }
}