﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using BananaPlanerMVC.Filters;
using SchoolDuesLib;
using SchoolDuesLib.Migrations;
using SchoolDuesLib.Models;

namespace BananaPlanerMVC
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
//            throw new Exception(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            ModelBinders.Binders.DefaultBinder = new PerpetuumSoft.Knockout.KnockoutModelBinder();
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

//            CheckDbStructure();
            MigrationInitializer.Run();
            InitializeSimpleMembershipAttribute.SimpleMembershipInitializer.CreateDb();
        }

        private void CheckDbStructure()
        {
//            Database.SetInitializer(new DbInitializer());
//            Database.SetInitializer(
//                new MigrateDatabaseToLatestVersion<BookContext, Configuration>());
// vvv
//            List<UserProfile> dbSet = new SiteContext().UserProfiles.ToList(); // force the create (debug only)
        }
    }
}