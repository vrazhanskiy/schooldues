﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using NLog;
using RazorEngine;
using SchoolDuesLib.Models;
using SchoolDuesLib.Utils;

namespace SchoolDuesLib.Email
{
    // todo make all methods internal so they can't be called from the web server
    public class SendEmailController
    {
        private const string ConfirmEmailSubject = "Confirm your email address";
        private const string CustomerPortalUserCreatedThankYouSubject = "You are invited to join PayTimePro";

        private static readonly string fromEmail = ConfigurationManager.AppSettings["emailFrom"];
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

	    public void SendConfirmCode(string email, string confirmCode, HttpRequest request)
        {
            string template = ReadTemplate("VerifyEmail.html");

            string result = Razor.Parse(template, new {email, confirmCode, BaseAddress = request.GetBaseUrl() });
            string toEmail = email;

            SendEmail( result, toEmail, ConfirmEmailSubject);
        }
        public void SendUsername(string username, string email, HttpRequest request = null)
        {
            string template = ReadTemplate("UsernameRecover.html");

            string result = Razor.Parse(template, new {username, BaseAddress = request.GetBaseUrl() });
            string toEmail = email;

            SendEmail( result, toEmail, "PayTimePRO : Username Recovery");
        }
        public void SendPassword(string password, string email, HttpRequest request)
        {
            string template = ReadTemplate("PasswordRecover.html");

            string result = Razor.Parse(template, new {password, BaseAddress = request.GetBaseUrl() });
            string toEmail = email;

            SendEmail( result, toEmail, "PayTimePRO : Password Recovery");
        }

        private static void SendEmail(string emailBody, string toEmail, string subject)
        {
            string emailPort = ConfigurationManager.AppSettings["emailPort"];
            SmtpClient client = string.IsNullOrWhiteSpace(emailPort)
                                    ? new SmtpClient(ConfigurationManager.AppSettings["emailServer"])
                                    : new SmtpClient(ConfigurationManager.AppSettings["emailServer"],
                                                     Convert.ToInt32(emailPort));

            MailMessage message = new MailMessage
            {
                IsBodyHtml = true,
                Subject = subject,
                Body = emailBody,
                From = new MailAddress(fromEmail, "Banana Planr Invite"),
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            };

            message.To.Add(new MailAddress(toEmail));

            client.DeliveryMethod = ConfigurationManager.AppSettings["emailDeliverMethod"] == "Network" ?
                SmtpDeliveryMethod.Network :
                SmtpDeliveryMethod.PickupDirectoryFromIis;

            string enableSsl = ConfigurationManager.AppSettings["emailEnableSsl"];
            if (enableSsl != null && bool.Parse(enableSsl))
            {
                client.EnableSsl = true;
            }

            client.Timeout = 10000;

            // Add credentials if the SMTP emailServer requires them.
            if (ConfigurationManager.AppSettings["emailUsername"] != null)
            {
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["emailUsername"],
                                                           ConfigurationManager.AppSettings["emailPassword"]);
            }

            if (bool.Parse(ConfigurationManager.AppSettings["SendEmails"]))
            {
                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    logger.ErrorException("Failed to send verification email ", ex);
                    throw;
                }
            }
        }

        private string ReadTemplate(string filename, HttpContext context = null)
        {
            if (context == null)
            {
                context = HttpContext.Current;
            }
            StringBuilder sb = new StringBuilder();
            logger.Debug("Filename={0}", filename);
            string fullPath = context.Server.MapPath("~/App_Data/EmailTemplates/" + filename);
            logger.Debug("Full Path {0}",fullPath);
            using (StreamReader sr = new StreamReader(fullPath))
            {
                String line;
                // Read and display lines from the file until the end of
                // the file is reached.
                while ((line = sr.ReadLine()) != null)
                {
                    sb.Append(line);
                }
                return sb.ToString();
            }
        }

        public void SendCustomerPortalUserCreatedThankYou(string ownerName, string email, string userid, string newPassword, HttpRequest request)
        {
            string template = ReadTemplate("CustomerPortalUserCreatedThankYou.html");

            string result = Razor.Parse(template, new { ownerName, email, confirmCode = userid, tempPassword = newPassword, BaseAddress = request.GetBaseUrl() });
            string toEmail = email;

            SendEmail( result, toEmail, CustomerPortalUserCreatedThankYouSubject);
        }

        public void SendInvite(Invitation invitation, string baseUrl)
        {
            try
            {
                string template = ReadTemplate("bp_invite.html");

                string result = Razor.Parse(template, new {baseUrl, invitation});
                string toEmail = invitation.Parent.Email;

                SendEmail(result, toEmail,
                    string.Format("BananaPlanr : Invitation to an Event \"{0}\"", invitation.Event.Name));
            }
            catch (Exception ex)
            {
                logger.ErrorException("Failed to send email ", ex);
                throw;
            }

        }

        public bool ProcessEmailToParticipatns(Event ev, IList<Invitation> invitations, string subject, string body,
                                               string baseUrl)
        {
            string template = ReadTemplate("bp_participant.html");

            bool errors = false;
            foreach (var invite in invitations)
            {
                try
                {
                    string result = Razor.Parse(template, new {baseUrl, body, ev, invite.Uid, invite.Parent.Email});

                    SendEmail(result, invite.Parent.Email, string.Format("BananaPlanr : {0}", subject));
                }
                catch (Exception ex)
                {
                    logger.ErrorException("Failed to send email ", ex);
                    errors = true;
                }
            }
            return errors;
        }

        public bool ProcessEmailToOrganizer(Invitation invite, string subject, string body, string baseUrl)
        {

            string template = ReadTemplate("bp_organizer.html");

            try
            {
                string result = Razor.Parse(template, new {baseUrl, body, invite});

                SendEmail(result, invite.Event.CreatedBy.Email, string.Format("BananaPlanr : {0}", subject));
            }
            catch (Exception ex)
            {
                logger.ErrorException("Failed to send email ", ex);
                return false;
            }
            return true;
        }
    }
}
