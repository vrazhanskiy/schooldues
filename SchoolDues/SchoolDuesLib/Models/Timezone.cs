﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDuesLib.Models
{
    public class Timezone
    {
        public int Id { get; set; }
        public int Order{ get; set; }
        public string SystemName{ get; set; }
        public string DisplayName{ get; set; }
    }
}
