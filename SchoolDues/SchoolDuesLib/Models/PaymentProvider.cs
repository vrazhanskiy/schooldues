namespace SchoolDuesLib.Models
{
    public enum PaymentProvider
    {
        Paypal, Dwolla, Stripe
    }
}