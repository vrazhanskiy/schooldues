using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SchoolDuesLib.Models
{
    public class VolunteerSpot
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        [DisplayName("Number of Volunteers")]
        public int NumberOfVolunteers { get; set; }
        public string Comments { get; set; }
        public virtual IList<UserProfile> Accepted { get; set; }
    }
}