﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolDuesLib.Models
{
    public class Event
    {
        public int Id { get; set; }
        public bool IsDraft { get; set; }

        [NotMapped]
        public bool SchoolRelated { get; set; }

        [ForeignKey("SchoolId")]
        public virtual School School { get; set; }
        
        [DisplayName("School")]
        public int? SchoolId { get; set; }

        [DisplayName("Teachers")]
        public string Teachers { get; set; }
        
        [Required]
        [DisplayName("Event Name")]
        public string Name { get; set; }

        [DisplayName("Event Description")]
        public string Description { get; set; }

        [DisplayName("Event Address/Location")]
        public string Location { get; set; }

        [Required]
        [DisplayName("Start Time")]
        public DateTime? StartTime { get; set; }

        [Required]
        [DisplayName("End Time")]
        public DateTime? EndTime { get; set; }

        [NotMapped]
        [Required]
        [DisplayName("Event Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }

        public UserProfile CreatedBy { get; set; }
        public UserProfile UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public int FundSelection { get; set; }
        [DisplayName("Amount per person")]
        public decimal? FundsPerPerson { get; set; }
        [DisplayName("Goal")]
        public decimal? FundsTotal { get; set; }


        // volunteer section
        [Required]
        [DisplayName("Volunteers")]
        public bool? NeedVolunteers { get; set; }
        public virtual IList<VolunteerSpot> VolunteerSpots { get; set; }

        // supplies section
        [Required]
        [DisplayName("Supplies")]
        public bool? NeedSupplies { get; set; }
        public virtual List<EventSupply> Supplies { get; set; } 
        
        // invitations
        public virtual List<Invitation> Invitations { get; set; }
        public virtual List<UserProfile> Admins { get; set; }

        public virtual List<UserPaymentAccount> AcceptablePayments { get; set; }
    }
}
 