using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolDuesLib.Models
{
    public class Order
    {
        public int Id { get; set; } // order
        public PaymentProvider Type { get; set; } // dwolla, stripe, paypal
        public int PaymentProviderId { get; set; } // reference to a raw message record
        public int ChildId { get; set; }

        public OrderStatus Status { get; set; }
    }

    public enum OrderStatus
    {
        Created, Complete
    }
}