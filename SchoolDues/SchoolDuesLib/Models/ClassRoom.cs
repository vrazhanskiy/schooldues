using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolDuesLib.Models
{
    public class ClassRoom  
    {
        [DisplayName("Class")]
        public int Id { get; set; }

        public string Grade { get; set; }
        public string RoomNumber { get; set; }
        public string Teacher { get; set; }
        public List<Student> Students { get; set; }

        [Required]
        [DisplayName("School")]
        public int SchoolId { get; set; }
        public School School { get; set; }
        [NotMapped]
        public string FullName { get { return string.Format("Grad:{0} Room:{1}", Grade, RoomNumber); } }
    }

}