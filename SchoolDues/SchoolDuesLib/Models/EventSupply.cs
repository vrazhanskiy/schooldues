namespace SchoolDuesLib.Models
{
    public class EventSupply
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public virtual UserProfile Accepted { get; set; }
    }
}