using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolDuesLib.Models
{
    [Table("Students")]
    public class Student:UserProfile
    {
        public ClassRoom Class { get; set; }
        public UserProfile Parent1{ get; set; }
        public UserProfile Parent2{ get; set; }

        [DisplayName("Date of Birth")]
        public DateTime? DateOfBirth { get; set; }
        // emergency contact

        [NotMapped]
        [DisplayName("First Name")]
        [Required]
        public string StudentFirstName
        {
            get { return FirstName; }
            set { FirstName = value; }
        }
        [NotMapped]
        [DisplayName("Last Name")]
        [Required]
        public string StudentLastName
        {
            get { return LastName; }
            set { LastName = value; }
        }

        [NotMapped]
        public int? SchoolId
        {
            get { return (Class != null) ? Class.SchoolId : ((int?) null); }
            set { if (Class != null)
            {
                Class.SchoolId = value?? 0;
            }}
        }
    }
}