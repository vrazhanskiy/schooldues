﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SchoolDuesLib.Models
{
    public class School
    {
        public int Id { get; set; }

        public List<ClassRoom> ClassRooms { get; set; }
        public List<UserProfile> Admins{ get; set; }
        public List<Student> Students { get; set; }

        [DisplayName("School Name")]
        public string Name { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }

        public string DepartmentOfEducationId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
