using Dwolla.Gateway;

namespace SchoolDuesLib.Models
{
    public class DwollaResponse: DwollaCallback
    {
        public int Id { get; set; }
        public string Error { get; set; }
        public string ClearingDate { get; set; }
        /*
        body	{ "Amount": 1.01, "CheckoutId": "f6a83749-312d-4a77-98ca-60f365314f06", "ClearingDate": "5/23/2013 12:00:00 AM", 
         * "Error": null, "OrderId": null, "Signature": "9659954086c646a693bcbc8bb6fabe7cd364544b", "Status": "Completed", 
         * "TestMode": "false", "TransactionId": 3019955 }*/

        /*
                body	{ "Amount": 1.02, "CheckoutId": "cffa6b5c-2f49-425d-b260-d1c920c7e4b1", "ClearingDate": "1/1/1970 12:00:00 AM", 
               "Error": null, "OrderId": "987654", "Signature": "2aa9721a36bf50e644233cd8b22442b60d96d051", "Status": "Completed", 
               "TestMode": "true", "TransactionId": null }

 
        body	{ "Amount": 1.0, "CheckoutId": "f2131461-4ce6-4be5-bf2b-05791c0d1ad6", "ClearingDate": "1/1/1970 12:00:00 AM", "Error": null, "OrderId": "Order123", "Signature": "f149b2bb97ec67a502fdbd7ba4c355e11461ba91", "Status": "Completed", "TestMode": "true", "TransactionId": null }
        */
    }
}