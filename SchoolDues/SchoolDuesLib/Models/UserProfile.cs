using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SchoolDuesLib.Models
{
    public class UserProfile
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string City{ get; set; }
        public string State { get; set; }

        public int? TimezoneId{ get; set; }
        [ForeignKey("TimezoneId")]
        public virtual Timezone Timezone { get; set; }

        [JsonIgnore]
        public virtual IList<School> UserSchools { get; set; }

        [JsonIgnore]
        public virtual IList<Event> Events { get; set; }
        [JsonIgnore]
        public virtual IList<VolunteerSpot> VolunteerSpots { get; set; }

        public virtual IList<UserPaymentAccount> PaymentAccounts { get; set; }
    }

/*
    public class TimeAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
                                                                            ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "time"
            };
        }

        public override bool IsValid(object value)
        {
            DateTime time;
            if (value == null || !DateTime.TryParse(value.ToString(), out time))
                return false;

            return true;
        }
    }
*/
}