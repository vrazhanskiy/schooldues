using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SchoolDuesLib.Models
{
    public class UserPaymentAccount
    {
        public int Id { get; set; }
        public PaymentProvider Provider { get; set; }
        [Required]
        public string Key { get; set; }
        [Required]
        public string Secret { get; set; }
        [Required]
        public string AccountId { get; set; }
        public IList<Event> Events { get; set; } // events that use this payment method
    }
}