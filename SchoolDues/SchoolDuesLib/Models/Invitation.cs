
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolDuesLib.Models
{
    public class Invitation
    {
        public int Id { get; set; }

        public int Event_Id { get; set; }
        [ForeignKey("Event_Id")]
        public virtual Event Event { get; set; }

        public virtual UserProfile Parent{ get; set; }
        public virtual UserProfile Student{ get; set; }
        public InvitationStatus Status { get; set; }
        public string Uid { get; set; }
        public DateTime? AcceptedOn { get; set; }

        public virtual Order Order { get; set; }
    }

    public enum InvitationStatus
    {
        Created, Sent, Viewed, Accepted, Rejected, PaymentPending
    }
}