using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using SchoolDuesLib.Models;

namespace SchoolDuesLib.Migrations
{
    public static class MigrationInitializer
    {
        public static void Run()
        {
//            Database.SetInitializer(new DropCreateDatabaseAlways<SiteContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SiteContext, Configuration>());
        }
    }

    internal sealed class Configuration : DbMigrationsConfiguration<SiteContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(SiteContext context)
        {
            //  This method will be called after migrating to the latest version.

            SeedTimezones(context);

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }

        private static void SeedTimezones(SiteContext context)
        {
            var timezones = new List<Timezone>
                {
                    new Timezone
                        {
                            Order = 10,
                            SystemName = "Dateline Standard Time",
                            DisplayName = "(UTC-12:00) International Date Line West"
                        },
                    new Timezone
                        {Order = 11, SystemName = "UTC-11", DisplayName = "(UTC-11:00) Coordinated Universal Time-11"},
                    new Timezone {Order = 0, SystemName = "Hawaiian Standard Time", DisplayName = "Hawaii"},
                    new Timezone {Order = 1, SystemName = "Alaskan Standard Time", DisplayName = "Alaska"},
                    new Timezone
                        {
                            Order = 12,
                            SystemName = "Pacific Standard Time (Mexico)",
                            DisplayName = "(UTC-08:00) Baja California"
                        },
                    new Timezone
                        {Order = 2, SystemName = "Pacific Standard Time", DisplayName = "Pacific Time (US & Canada)"},
                    new Timezone {Order = 3, SystemName = "US Mountain Standard Time", DisplayName = "Arizona"},
                    new Timezone
                        {
                            Order = 13,
                            SystemName = "Mountain Standard Time (Mexico)",
                            DisplayName = "(UTC-07:00) Chihuahua, La Paz, Mazatlan"
                        },
                    new Timezone
                        {Order = 4, SystemName = "Mountain Standard Time", DisplayName = "Mountain Time (US & Canada)"},
                    new Timezone
                        {
                            Order = 14,
                            SystemName = "Central America Standard Time",
                            DisplayName = "(UTC-06:00) Central America"
                        },
                    new Timezone
                        {Order = 5, SystemName = "Central Standard Time", DisplayName = "Central Time (US & Canada)"},
                    new Timezone
                        {
                            Order = 15,
                            SystemName = "Central Standard Time (Mexico)",
                            DisplayName = "(UTC-06:00) Guadalajara, Mexico City, Monterrey"
                        },
                    new Timezone
                        {
                            Order = 16,
                            SystemName = "Canada Central Standard Time",
                            DisplayName = "(UTC-06:00) Saskatchewan"
                        },
                    new Timezone
                        {
                            Order = 17,
                            SystemName = "SA Pacific Standard Time",
                            DisplayName = "(UTC-05:00) Bogota, Lima, Quito"
                        },
                    new Timezone
                        {Order = 6, SystemName = "Eastern Standard Time", DisplayName = "Eastern Time (US & Canada)"},
                    new Timezone {Order = 7, SystemName = "US Eastern Standard Time", DisplayName = "Indiana (East)"},
                    new Timezone
                        {Order = 18, SystemName = "Venezuela Standard Time", DisplayName = "(UTC-04:30) Caracas"},
                    new Timezone
                        {Order = 19, SystemName = "Paraguay Standard Time", DisplayName = "(UTC-04:00) Asuncion"},
                    new Timezone
                        {Order = 8, SystemName = "Atlantic Standard Time", DisplayName = "Atlantic Time (Canada)"},
                    new Timezone
                        {Order = 20, SystemName = "Central Brazilian Standard Time", DisplayName = "(UTC-04:00) Cuiaba"},
                    new Timezone
                        {
                            Order = 21,
                            SystemName = "SA Western Standard Time",
                            DisplayName = "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan"
                        },
                    new Timezone
                        {Order = 22, SystemName = "Pacific SA Standard Time", DisplayName = "(UTC-04:00) Santiago"},
                    new Timezone {Order = 9, SystemName = "Newfoundland Standard Time", DisplayName = "Newfoundland"},
                    new Timezone
                        {
                            Order = 23,
                            SystemName = "E. South America Standard Time",
                            DisplayName = "(UTC-03:00) Brasilia"
                        },
                    new Timezone
                        {Order = 24, SystemName = "Argentina Standard Time", DisplayName = "(UTC-03:00) Buenos Aires"},
                    new Timezone
                        {
                            Order = 25,
                            SystemName = "SA Eastern Standard Time",
                            DisplayName = "(UTC-03:00) Cayenne, Fortaleza"
                        },
                    new Timezone
                        {Order = 26, SystemName = "Greenland Standard Time", DisplayName = "(UTC-03:00) Greenland"},
                    new Timezone
                        {Order = 27, SystemName = "Montevideo Standard Time", DisplayName = "(UTC-03:00) Montevideo"},
                    new Timezone {Order = 28, SystemName = "Bahia Standard Time", DisplayName = "(UTC-03:00) Salvador"},
                    new Timezone
                        {Order = 29, SystemName = "UTC-02", DisplayName = "(UTC-02:00) Coordinated Universal Time-02"},
                    new Timezone
                        {
                            Order = 30,
                            SystemName = "Mid-Atlantic Standard Time",
                            DisplayName = "(UTC-02:00) Mid-Atlantic"
                        },
                    new Timezone {Order = 31, SystemName = "Azores Standard Time", DisplayName = "(UTC-01:00) Azores"},
                    new Timezone
                        {
                            Order = 32,
                            SystemName = "Cape Verde Standard Time",
                            DisplayName = "(UTC-01:00) Cape Verde Is."
                        },
                    new Timezone {Order = 33, SystemName = "Morocco Standard Time", DisplayName = "(UTC) Casablanca"},
                    new Timezone {Order = 34, SystemName = "UTC", DisplayName = "(UTC) Coordinated Universal Time"},
                    new Timezone
                        {
                            Order = 35,
                            SystemName = "GMT Standard Time",
                            DisplayName = "(UTC) Dublin, Edinburgh, Lisbon, London"
                        },
                    new Timezone
                        {Order = 36, SystemName = "Greenwich Standard Time", DisplayName = "(UTC) Monrovia, Reykjavik"},
                    new Timezone
                        {
                            Order = 37,
                            SystemName = "W. Europe Standard Time",
                            DisplayName = "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"
                        },
                    new Timezone
                        {
                            Order = 38,
                            SystemName = "Central Europe Standard Time",
                            DisplayName = "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"
                        },
                    new Timezone
                        {
                            Order = 39,
                            SystemName = "Romance Standard Time",
                            DisplayName = "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris"
                        },
                    new Timezone
                        {
                            Order = 40,
                            SystemName = "Central European Standard Time",
                            DisplayName = "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb"
                        },
                    new Timezone
                        {
                            Order = 41,
                            SystemName = "W. Central Africa Standard Time",
                            DisplayName = "(UTC+01:00) West Central Africa"
                        },
                    new Timezone
                        {Order = 42, SystemName = "Namibia Standard Time", DisplayName = "(UTC+01:00) Windhoek"},
                    new Timezone {Order = 43, SystemName = "Jordan Standard Time", DisplayName = "(UTC+02:00) Amman"},
                    new Timezone
                        {Order = 44, SystemName = "GTB Standard Time", DisplayName = "(UTC+02:00) Athens, Bucharest"},
                    new Timezone
                        {Order = 45, SystemName = "Middle East Standard Time", DisplayName = "(UTC+02:00) Beirut"},
                    new Timezone {Order = 46, SystemName = "Egypt Standard Time", DisplayName = "(UTC+02:00) Cairo"},
                    new Timezone {Order = 47, SystemName = "Syria Standard Time", DisplayName = "(UTC+02:00) Damascus"},
                    new Timezone
                        {
                            Order = 48,
                            SystemName = "South Africa Standard Time",
                            DisplayName = "(UTC+02:00) Harare, Pretoria"
                        },
                    new Timezone
                        {
                            Order = 49,
                            SystemName = "FLE Standard Time",
                            DisplayName = "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"
                        },
                    new Timezone {Order = 50, SystemName = "Turkey Standard Time", DisplayName = "(UTC+02:00) Istanbul"},
                    new Timezone
                        {Order = 51, SystemName = "Israel Standard Time", DisplayName = "(UTC+02:00) Jerusalem"},
                    new Timezone
                        {Order = 52, SystemName = "E. Europe Standard Time", DisplayName = "(UTC+02:00) Nicosia"},
                    new Timezone {Order = 53, SystemName = "Arabic Standard Time", DisplayName = "(UTC+03:00) Baghdad"},
                    new Timezone
                        {
                            Order = 54,
                            SystemName = "Kaliningrad Standard Time",
                            DisplayName = "(UTC+03:00) Kaliningrad, Minsk"
                        },
                    new Timezone
                        {Order = 55, SystemName = "Arab Standard Time", DisplayName = "(UTC+03:00) Kuwait, Riyadh"},
                    new Timezone
                        {Order = 56, SystemName = "E. Africa Standard Time", DisplayName = "(UTC+03:00) Nairobi"},
                    new Timezone {Order = 57, SystemName = "Iran Standard Time", DisplayName = "(UTC+03:30) Tehran"},
                    new Timezone
                        {
                            Order = 58,
                            SystemName = "Arabian Standard Time",
                            DisplayName = "(UTC+04:00) Abu Dhabi, Muscat"
                        },
                    new Timezone {Order = 59, SystemName = "Azerbaijan Standard Time", DisplayName = "(UTC+04:00) Baku"},
                    new Timezone
                        {
                            Order = 60,
                            SystemName = "Russian Standard Time",
                            DisplayName = "(UTC+04:00) Moscow, St. Petersburg, Volgograd"
                        },
                    new Timezone
                        {Order = 61, SystemName = "Mauritius Standard Time", DisplayName = "(UTC+04:00) Port Louis"},
                    new Timezone
                        {Order = 62, SystemName = "Georgian Standard Time", DisplayName = "(UTC+04:00) Tbilisi"},
                    new Timezone
                        {Order = 63, SystemName = "Caucasus Standard Time", DisplayName = "(UTC+04:00) Yerevan"},
                    new Timezone
                        {Order = 64, SystemName = "Afghanistan Standard Time", DisplayName = "(UTC+04:30) Kabul"},
                    new Timezone
                        {
                            Order = 65,
                            SystemName = "Pakistan Standard Time",
                            DisplayName = "(UTC+05:00) Islamabad, Karachi"
                        },
                    new Timezone
                        {Order = 66, SystemName = "West Asia Standard Time", DisplayName = "(UTC+05:00) Tashkent"},
                    new Timezone
                        {
                            Order = 67,
                            SystemName = "India Standard Time",
                            DisplayName = "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi"
                        },
                    new Timezone
                        {
                            Order = 68,
                            SystemName = "Sri Lanka Standard Time",
                            DisplayName = "(UTC+05:30) Sri Jayawardenepura"
                        },
                    new Timezone {Order = 69, SystemName = "Nepal Standard Time", DisplayName = "(UTC+05:45) Kathmandu"},
                    new Timezone
                        {Order = 70, SystemName = "Central Asia Standard Time", DisplayName = "(UTC+06:00) Astana"},
                    new Timezone
                        {Order = 71, SystemName = "Bangladesh Standard Time", DisplayName = "(UTC+06:00) Dhaka"},
                    new Timezone
                        {
                            Order = 72,
                            SystemName = "Ekaterinburg Standard Time",
                            DisplayName = "(UTC+06:00) Ekaterinburg"
                        },
                    new Timezone
                        {Order = 73, SystemName = "Myanmar Standard Time", DisplayName = "(UTC+06:30) Yangon (Rangoon)"},
                    new Timezone
                        {
                            Order = 74,
                            SystemName = "SE Asia Standard Time",
                            DisplayName = "(UTC+07:00) Bangkok, Hanoi, Jakarta"
                        },
                    new Timezone
                        {
                            Order = 75,
                            SystemName = "N. Central Asia Standard Time",
                            DisplayName = "(UTC+07:00) Novosibirsk"
                        },
                    new Timezone
                        {
                            Order = 76,
                            SystemName = "China Standard Time",
                            DisplayName = "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi"
                        },
                    new Timezone
                        {Order = 77, SystemName = "North Asia Standard Time", DisplayName = "(UTC+08:00) Krasnoyarsk"},
                    new Timezone
                        {
                            Order = 78,
                            SystemName = "Singapore Standard Time",
                            DisplayName = "(UTC+08:00) Kuala Lumpur, Singapore"
                        },
                    new Timezone
                        {Order = 79, SystemName = "W. Australia Standard Time", DisplayName = "(UTC+08:00) Perth"},
                    new Timezone {Order = 80, SystemName = "Taipei Standard Time", DisplayName = "(UTC+08:00) Taipei"},
                    new Timezone
                        {Order = 81, SystemName = "Ulaanbaatar Standard Time", DisplayName = "(UTC+08:00) Ulaanbaatar"},
                    new Timezone
                        {Order = 82, SystemName = "North Asia East Standard Time", DisplayName = "(UTC+09:00) Irkutsk"},
                    new Timezone
                        {
                            Order = 83,
                            SystemName = "Tokyo Standard Time",
                            DisplayName = "(UTC+09:00) Osaka, Sapporo, Tokyo"
                        },
                    new Timezone {Order = 84, SystemName = "Korea Standard Time", DisplayName = "(UTC+09:00) Seoul"},
                    new Timezone
                        {Order = 85, SystemName = "Cen. Australia Standard Time", DisplayName = "(UTC+09:30) Adelaide"},
                    new Timezone
                        {Order = 86, SystemName = "AUS Central Standard Time", DisplayName = "(UTC+09:30) Darwin"},
                    new Timezone
                        {Order = 87, SystemName = "E. Australia Standard Time", DisplayName = "(UTC+10:00) Brisbane"},
                    new Timezone
                        {
                            Order = 88,
                            SystemName = "AUS Eastern Standard Time",
                            DisplayName = "(UTC+10:00) Canberra, Melbourne, Sydney"
                        },
                    new Timezone
                        {
                            Order = 89,
                            SystemName = "West Pacific Standard Time",
                            DisplayName = "(UTC+10:00) Guam, Port Moresby"
                        },
                    new Timezone {Order = 90, SystemName = "Tasmania Standard Time", DisplayName = "(UTC+10:00) Hobart"},
                    new Timezone {Order = 91, SystemName = "Yakutsk Standard Time", DisplayName = "(UTC+10:00) Yakutsk"},
                    new Timezone
                        {
                            Order = 92,
                            SystemName = "Central Pacific Standard Time",
                            DisplayName = "(UTC+11:00) Solomon Is., New Caledonia"
                        },
                    new Timezone
                        {Order = 93, SystemName = "Vladivostok Standard Time", DisplayName = "(UTC+11:00) Vladivostok"},
                    new Timezone
                        {
                            Order = 94,
                            SystemName = "New Zealand Standard Time",
                            DisplayName = "(UTC+12:00) Auckland, Wellington"
                        },
                    new Timezone
                        {Order = 95, SystemName = "UTC+12", DisplayName = "(UTC+12:00) Coordinated Universal Time+12"},
                    new Timezone {Order = 96, SystemName = "Fiji Standard Time", DisplayName = "(UTC+12:00) Fiji"},
                    new Timezone {Order = 97, SystemName = "Magadan Standard Time", DisplayName = "(UTC+12:00) Magadan"},
                    new Timezone
                        {
                            Order = 98,
                            SystemName = "Kamchatka Standard Time",
                            DisplayName = "(UTC+12:00) Petropavlovsk-Kamchatsky - Old"
                        },
                    new Timezone
                        {Order = 99, SystemName = "Tonga Standard Time", DisplayName = "(UTC+13:00) Nuku'alofa"},
                    new Timezone {Order = 100, SystemName = "Samoa Standard Time", DisplayName = "(UTC+13:00) Samoa"}
                };
            
//            IDbSetExtensions.AddOrUpdate(context.Timezones, t => t.TimezoneId, new Timezone());
            IDbSetExtensions.AddOrUpdate(context.Timezones, t => t.SystemName, timezones.ToArray());
        }
    }
}