using System.Data.Entity;
using System.Linq;

namespace SchoolDuesLib.Providers
{
    public class BaseDataAccess
    {
        protected string Username { get; private set; }
        protected readonly SiteContext DbContext;

        public BaseDataAccess(string username)
        {
            Username = username;
            DbContext= new SiteContext(username);
        }

        public BaseDataAccess(SiteContext dbContext)
        {
            DbContext = dbContext;
        }
    }
    
}