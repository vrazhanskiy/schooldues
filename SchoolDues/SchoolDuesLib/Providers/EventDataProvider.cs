﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NLog;
using SchoolDuesLib.Email;
using SchoolDuesLib.Models;
using SchoolDuesLib.Utils;

namespace SchoolDuesLib.Providers
{
    public class EventDataProvider:BaseDataAccess
    {
//        private readonly string userName;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public EventDataProvider(string userName)
            : base(userName)
        {
//            this.userName = userName;
        }

        public Event CreateEvent(Event ev, string userName)
        {
            DbContext.Events.Add(ev);
            ev.School = ev.SchoolId != null ? new SchoolDataProvider(DbContext).GetSchool(ev.SchoolId.Value) : null;
            
            ev.CreatedBy = new UserDataAccess(DbContext).GetUserProfileByUserName(userName);
            ev.Admins = new List<UserProfile> { ev.CreatedBy };

            if (ev.CreatedBy.PaymentAccounts.Any())
            {
                ev.AcceptablePayments.AddRange(ev.CreatedBy.PaymentAccounts);// for now add all
            }
            else
            {
                ev.FundsPerPerson = null; // just in case somebody tries to mofiy the model manually in html let's just zero this out.
                ev.FundsTotal = null;
            }

            UpdateInvitesBasedOnChildren(ev, ev, userName);

            return ev;
        }

        public List<Event> GetEventsCanAdmin(string userNameParam = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            if (string.IsNullOrWhiteSpace(userNameParam))
            {
                userNameParam = Username;
            }

            if (fromDate == null)
            {
                fromDate = DateTime.Today;
            }
            var userOwned = DbContext.Events
                .Where(e => e.Admins.Select(a => a.UserName).Contains(userNameParam) && e.StartTime >= fromDate);
            
            if (toDate != null)
            {
                userOwned = userOwned.Where(e => e.StartTime <= toDate);
            }

            var eventsCanAdmin = userOwned.ToList();
            eventsCanAdmin.UpdateToUserTimezone(userNameParam);
            return eventsCanAdmin;
        }

        public Event GetEventAsAdmin(int eventId, string userName = null)
        {
            if (userName == null)
            {
                userName = this.Username;
            }
            var ev = DbContext.Events.FirstOrDefault(e => e.Id == eventId);
            if (ev != null && ev.Admins.Select(a=>a.UserName).Contains(userName))
            {
                ev.UpdateToUserTimezone(userName);
                return ev;
            }
            throw new Exception("Not Authorized");
        }

        public Event UpdateEvent(Event ev, string userName)
        {
            var dbEvent = GetEventAsAdmin(ev.Id, userName);

/*
            // copy only the first screen data. 
            dbEvent.SchoolRelated = ev.SchoolRelated;
            if (ev.SchoolId != null)
            {
                dbEvent.School = new SchoolDataProvider(DbContext).GetSchool(ev.SchoolId.Value);
            }
            else
            {
                var school = dbEvent.School; // is this the best to null a lazy loadable property?
                dbEvent.School = null;
            }
*/
            dbEvent.Teachers = ev.Teachers;
            dbEvent.Name = ev.Name;
            dbEvent.Description = ev.Description;
            dbEvent.StartTime = ev.StartTime;
            dbEvent.EndTime= ev.EndTime;
            dbEvent.Location = ev.Location;
            dbEvent.FundsPerPerson = ev.FundsPerPerson;
            dbEvent.FundsTotal = ev.FundsTotal;
            dbEvent.NeedVolunteers = ev.NeedVolunteers;
            dbEvent.NeedSupplies = ev.NeedSupplies;

            UpdateInvitesBasedOnChildren(dbEvent, ev, userName);
            return dbEvent;
        }

        private void UpdateInvitesBasedOnChildren(Event dbEvent, Event ev, string userName)
        {
            // first create invite for the creator
            var userInvite = dbEvent.Invitations.FirstOrDefault(i => i.Parent.UserName == userName);
            var userDataAccess = new UserDataAccess(DbContext);
            if (userInvite == null)
            {
                userInvite = new Invitation
                {
                    Status = InvitationStatus.Created, 
                    Parent = userDataAccess.GetUserProfileByUserName(userName),
                    Uid = CreateNewInvitationId()
                };
                dbEvent.Invitations.Add(userInvite);
            }
            DbContext.SaveChanges();
        }

        public Event UpdateEventVolunteers(Event model, string userName)
        {
            var dbEvent = GetEventAsAdmin(model.Id, userName);

            // zero means delete
            var newVolSpots = model.VolunteerSpots.Where(v=>v.NumberOfVolunteers != 0).ToList();

            int numOfSpotsToAdd = newVolSpots.Count - dbEvent.VolunteerSpots.Count;
            if (numOfSpotsToAdd > 0)
            {
                for (int i = 0; i < numOfSpotsToAdd; i++)
                {
                    dbEvent.VolunteerSpots.Add(new VolunteerSpot());
                }
            } 
            else if (numOfSpotsToAdd < 0)
            {
                foreach (var spotToDelete in dbEvent.VolunteerSpots.Skip(newVolSpots.Count).ToList())
                {
                    dbEvent.VolunteerSpots.Remove(spotToDelete);
                    DbContext.VolunteerSpots.Remove(spotToDelete);
                }    
            }

            for (int i = 0; i < newVolSpots.Count; i++)
            {
                dbEvent.VolunteerSpots[i].NumberOfVolunteers = newVolSpots[i].NumberOfVolunteers;
                dbEvent.VolunteerSpots[i].StartTime = newVolSpots[i].StartTime;
                dbEvent.VolunteerSpots[i].EndTime = newVolSpots[i].EndTime;
                dbEvent.VolunteerSpots[i].Comments = newVolSpots[i].Comments;
            }

            DbContext.SaveChanges();

            return dbEvent;
        }

        public void UpdateSupplies(Event model, string[] supplies, string userName)
        {
            var dbEvent = GetEventAsAdmin(model.Id, userName);
            if (dbEvent.Supplies != null)
            {
                var eventSupplies = dbEvent.Supplies.ToList();
                foreach (var eventSupply in eventSupplies)
                {
                    dbEvent.Supplies.Remove(eventSupply);
                    DbContext.EventSupplies.Remove(eventSupply);
                }
            }
            else
            {
                dbEvent.Supplies= new List<EventSupply>();
            }
            foreach (var supply in supplies)
            {
                dbEvent.Supplies.Add(new EventSupply { Description = supply });
            }

            DbContext.SaveChanges();
        }

        public void UpdateInvites(Event model, string[] newEmails, string userName, string baseUrl)
        {
            var dbEvent = GetEventAsAdmin(model.Id, userName);
            if (dbEvent.Invitations == null)
            {
                dbEvent.Invitations = new List<Invitation>();
            }


            // delete those not on a list any more
            foreach (var inviteToDelete in dbEvent.Invitations.Where(i => !newEmails.Contains(i.Parent.Email)).ToList())
            {
                dbEvent.Invitations.Remove(inviteToDelete);
                DbContext.Invitations.Remove(inviteToDelete);
            }

            var userDataAccess = new UserDataAccess(DbContext);

            // add new people
            IList<Invitation> newInvites = new List<Invitation>();
            var existingEmails = dbEvent.Invitations.Select(i => i.Parent.Email).ToList();
            foreach (var ivniteEmail in newEmails.Where(e=>!existingEmails.Contains(e)))
            {
                var newInvite = new Invitation
                    {
                        Status = InvitationStatus.Created,
                        Parent = userDataAccess.FindOrCreateByEmail(ivniteEmail),
                        Uid = CreateNewInvitationId()
                    };
                newInvites.Add(newInvite);
                dbEvent.Invitations.Add(newInvite);
            }
            DbContext.SaveChanges();
            if (!dbEvent.IsDraft)
            {
                SendInvites(newInvites, baseUrl);
            }
            userDataAccess.MakeSureRole(dbEvent.Invitations.Select(i => i.Parent.UserName), Constants.ParentRoleName);
        }

        public void UpdateAdmins(Event model, string[] newEmails, string userName)
        {
            var dbEvent = GetEventAsAdmin(model.Id, userName);
            Debug.Assert(dbEvent.Admins != null);

            // delete those not on a list any more
            foreach (var adminsToDelete in dbEvent.Admins.Where(a => !newEmails.Contains(a.Email) && a.UserName != userName).ToList())
            {
                dbEvent.Admins.Remove(adminsToDelete);
            }

            var userDataAccess = new UserDataAccess(DbContext);

            // add new people
            var existingEmails = dbEvent.Admins.Select(a => a.Email).ToList();
            foreach (var adminEmail in newEmails.Where(e => !existingEmails.Contains(e)))
            {
                dbEvent.Admins.Add(userDataAccess.FindOrCreateByEmail(adminEmail));
            }

//            dbEvent.UpdateToUniversalTime(userName);
            DbContext.SaveChanges();
            userDataAccess.MakeSureRole(dbEvent.Invitations.Select(i => i.Parent.UserName), Constants.EventAdminRoleName);
        }

        public Event Publish(int eventId, string userName, string baseUrl)
        {
            logger.Info("Publishing event {0}", eventId);
            var dbEvent = GetEventAsAdmin(eventId, userName);
            foreach (var invitation in dbEvent.Invitations.Where(i=>i.Status == InvitationStatus.Created))
            {
                invitation.Uid = CreateNewInvitationId();
                invitation.Status = InvitationStatus.Sent;
            }
            dbEvent.IsDraft = false;
            DbContext.SaveChanges();

            logger.Info("Starting a thread to send emails for eventId {0}", eventId);
            SendInvites(dbEvent.Invitations, baseUrl);
            return dbEvent;
        }

        private static string CreateNewInvitationId()
        {
            return Guid.NewGuid().ToString().Replace("-","");
        }

        private void SendInvites(IEnumerable<Invitation> invitations, string baseUrl)
        {
            var emailController = new SendEmailController();
            foreach (var invitation in invitations)
            {
                try
                {
                    emailController.SendInvite(invitation, baseUrl);
                }
                catch (Exception e)
                {
                    logger.Error("Failed to send invite to {0} for invitationId {1}", invitation.Parent.Email, invitation.Id);
                }
            }
        }

        public IList<Invitation> GetEventsForParent(DateTime from, DateTime to, string username)
        {
            to = to.ToUserUtcTime(username);
            from = from.ToUserUtcTime(username);
            List<Invitation> invitations = DbContext.Invitations.Where(i => i.Parent.UserName == username && i.Event.StartTime >= @from && i.Event.StartTime <= to).ToList();

            invitations.Select(i=>i.Event).ToList().ForEach(e=>e.UpdateToUserTimezone(username));

            return invitations;
        }
    }
}
