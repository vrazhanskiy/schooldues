﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using SchoolDuesLib.Models;

namespace SchoolDuesLib.Providers
{
    public class UserDataAccess:BaseDataAccess
    {
        public UserDataAccess(string username) : base(username)
        {
        }

        public UserDataAccess(SiteContext dbContext) : base(dbContext)
        {
        }

        public void CreateUser(UserProfile userProfile)
        {
            var found = DbContext.UserProfiles.FirstOrDefault(u => u.Email == userProfile.Email);
            if (found == null)
            {
                DbContext.UserProfiles.Add(userProfile);
                DbContext.SaveChanges();
            }
            else
            {
                throw new Exception("User with this email already exists");
            }
        }
        public void UpdateUser(UserProfile userProfile)
        {
            var found = DbContext.UserProfiles.FirstOrDefault(u => u.Email == userProfile.Email);
            if (found == null)
            {
                throw new Exception("User not found");
            }
            else
            {
                found.FirstName = userProfile.FirstName;
                found.LastName = userProfile.LastName;
                found.MiddleInitial = userProfile.MiddleInitial;
                found.PhoneNumber = userProfile.PhoneNumber;
                found.TimezoneId = userProfile.TimezoneId;
            }
            DbContext.SaveChanges();
        }

        public UserProfile GetUserProfileByUserName(string userName)
        {
            return DbContext.UserProfiles.FirstOrDefault(u => u.UserName == userName);
        }

        public void AddSchoolToProfile(UserProfile userProfile, School school)
        {
            var found = DbContext.UserProfiles.Include("UserSchools").FirstOrDefault(u => u.UserName == userProfile.UserName);
            if(found!= null)
            {
                var schoolDataProvider = new SchoolDataProvider(DbContext);
                var dbSchool = schoolDataProvider.GetOrCreateSchool(school.Id, school.Name, school.City, school.State);
                
                if (found.UserSchools == null)
                {
                    found.UserSchools = new List<School>();
                }
                if (found.UserSchools.All(s => s.Id != school.Id))
                {
                    found.UserSchools.Add(dbSchool);
                }
                DbContext.SaveChanges();
            }
        }

        public void RemoveSchoolFromProfile(UserProfile userProfile, int schoolId)
        {
            var found = DbContext.UserProfiles.Include("UserSchools").FirstOrDefault(u => u.UserName == userProfile.UserName);
            if (found != null)
            {
                var foundSchool = found.UserSchools.FirstOrDefault(s => s.Id == schoolId);
                if (foundSchool != null)
                {
                    found.UserSchools.Remove(foundSchool);
                    DbContext.SaveChanges();
                }
            }
        }

        public UserProfile GetUserProfileByEmailAddress(string email)
        {
            return DbContext.UserProfiles.FirstOrDefault(u => u.Email == email);
        }

        // NOTE: Does not save to db. it's a caller responsibility to finish the transaction.
        public UserProfile FindOrCreateByEmail(string email)
        {
            var found = GetUserProfileByEmailAddress(email);
            if (found == null)
            {
                found = new UserProfile
                    {
                        Email = email,
                        UserName = Guid.NewGuid().ToString()
                    };
                DbContext.UserProfiles.Add(found);
            }
            return found;
        }

        public void MakeSureRole(IEnumerable<string> parentUserNames, string role)
        {
            foreach (var parentUserName in parentUserNames)
            {
                if (!Roles.IsUserInRole(parentUserName, role))
                {
                    Roles.AddUserToRole(parentUserName, role);
                }
            }
        }

        public UserPaymentAccount AddUpdateDwolla(UserProfile dbUser, UserPaymentAccount userPaymentAccount)
        {
            var found = dbUser.PaymentAccounts.FirstOrDefault(p => p.Provider == PaymentProvider.Dwolla);
            if (found == null)
            {
                found = new UserPaymentAccount
                    {
                        Provider =  PaymentProvider.Dwolla
                    };
                dbUser.PaymentAccounts.Add(found);
            }

            found.Key = userPaymentAccount.Key;
            found.AccountId = userPaymentAccount.AccountId;
            found.Secret = userPaymentAccount.Secret;
            DbContext.SaveChanges();
            return found;
        }
    }
}
