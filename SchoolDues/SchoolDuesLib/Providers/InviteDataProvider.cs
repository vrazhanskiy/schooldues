﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using SchoolDuesLib.Exceptions;
using SchoolDuesLib.Models;
using SchoolDuesLib.Utils;

namespace SchoolDuesLib.Providers
{
    public class InviteDataProvider : BaseDataAccess
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public InviteDataProvider(string username) : base(username)
        {
        }

        public Invitation GetByInviteId(string inviteId)
        {
            var found = DbContext.Invitations.Include("Event.CreatedBy").FirstOrDefault(i => i.Uid == inviteId);
            if (found != null)
            {
                if (found.Status == InvitationStatus.Created)
                {
                    found.Status = InvitationStatus.Viewed;
                }
                DbContext.SaveChanges();

                found.Event.UpdateToUserTimezone(found.Parent.UserName);
                return found;
            }
            return null;
        }

        public Invitation GetInvite(int inviteId, string userName = null)
        {
            if (userName == null)
            {
                userName = Username;
            }
            Invitation firstOrDefault = DbContext.Invitations.Include("Event.CreatedBy").FirstOrDefault(i => i.Parent.UserName == userName && i.Id == inviteId);
            if (firstOrDefault != null)
            {
                firstOrDefault.Event.UpdateToUserTimezone(userName);
            }
            return firstOrDefault;
        }

        public List<Invitation> GetInvitesForParent(string userName)
        {
            List<Invitation> invitesForParent = DbContext.Invitations.Where(i => i.Parent.UserName == userName && !i.Event.IsDraft && i.Event.StartTime > DateTime.Now).ToList();
            invitesForParent.Select(i=>i.Event).ToList().ForEach(e=>e.UpdateToUserTimezone(userName));
            return
                invitesForParent;
        }

        public Invitation AcceptSupply(int supplyId, string userName)
        {
            // check permissions
            var dbEvent =
                DbContext.Events.FirstOrDefault(
                    e => e.Supplies.Any(s => s.Id == supplyId) && e.Invitations.Any(i => i.Parent.UserName == userName));
            if (dbEvent == null)
            {
                throw new NotAuthorizedException();
            }

            var eventSupply = DbContext.EventSupplies.First(s => s.Id == supplyId);
            var userProfile = new UserDataAccess(DbContext).GetUserProfileByUserName(userName);
            eventSupply.Accepted = userProfile;
            DbContext.SaveChanges();

            return dbEvent.Invitations.First(i => i.Parent.UserName == userName);
        }

/*
        public void AcceptSupplyGuest(int supplyId, string inviteUId)
        {
            // check permissions

            var dbEvent =
                DbContext.Events.FirstOrDefault(
                    e => e.Supplies.Any(s => s.Id == supplyId) && e.Invitations.Any(i => i.Uid == inviteUId));
            if (dbEvent == null)
            {
                throw new NotAuthorizedException();
            }

            var eventSupply = DbContext.EventSupplies.First(s => s.Id == supplyId);
            var invitation = DbContext.Invitations.First(i => i.Uid == inviteUId);

            var userProfile = new UserDataAccess(DbContext).GetUserProfileByUserName(invitation.Parent.UserName);
            eventSupply.Accepted = userProfile;
            DbContext.SaveChanges();
        }

*/

        public Invitation AcceptVolunteer(int volId, string userName)
        {
            var dbEvent = DbContext.Events.FirstOrDefault(
                e => e.VolunteerSpots.Any(v => v.Id == volId) && e.Invitations.Any(i => i.Parent.UserName == userName));
            if (dbEvent == null)
            {
                throw new NotAuthorizedException();
            }

            var volsSpot = DbContext.VolunteerSpots.First(v => v.Id == volId);

            if (volsSpot.Accepted.All(v => v.UserName != userName))
            {
                var userProfile = new UserDataAccess(DbContext).GetUserProfileByUserName(userName);
                volsSpot.Accepted.Add(userProfile);
                DbContext.SaveChanges();
            }

            return dbEvent.Invitations.First(i => i.Parent.UserName == userName);
        }

        public List<Invitation> GetInvitesByChildId(int childId, string userName)
        {
            var child =
                new ChildDataAccess(DbContext).GetStudentsByParentUsername(userName).FirstOrDefault(c => c.Id == childId);
            if (child == null)
            {
                throw new NotAuthorizedException();
            }

            List<Invitation> invitesByChildId = DbContext.Invitations.Where(e => e.Student.Id == child.Id).ToList();
            invitesByChildId.Select(i=>i.Event).ToList().ForEach(e=>e.UpdateToUserTimezone(userName));
            return invitesByChildId;
        }

        public Order CreateOrGet(Invitation invitation, int childId, PaymentProvider type)
        {
            if (invitation.Order == null)
            {
                invitation.Order = new Order
                    {
                        Type = type,
                        Status = OrderStatus.Created,
                        ChildId = childId
                    };
            }
            else if (invitation.Order.Status != OrderStatus.Complete)
            {
                invitation.Order.ChildId = childId;
            }
            DbContext.SaveChanges();
            return invitation.Order;
        }

        public Invitation AcceptInvite(int id, int childId, string userName = null)
        {
            if (userName == null)
            {
                userName = this.Username;
            }
            var child =
                new ChildDataAccess(DbContext).GetStudentsByParentUsername(userName).FirstOrDefault(c => c.Id == childId);
            if (child == null)
            {
                throw new NotAuthorizedException();
            }
            var invite = GetInvite(id, userName);
            if (invite != null)
            {
                invite.Student = child;
                invite.Status = InvitationStatus.Accepted;
                invite.AcceptedOn = DateTime.Now;
            }
            DbContext.SaveChanges();
            invite.Event.UpdateToUserTimezone(userName);
            return invite;
        }

        public Order GetOrderFromDwollaResponse(DwollaResponse response)
        {
            int orderId;
            if (int.TryParse(response.OrderId, out orderId))
            {
                return DbContext.Orders.FirstOrDefault(o => o.Id == orderId);
            }
            return null;
        }

        public void ProcessOrderAndAcceptInvite(Order order)
        {
            if (order != null)
            {
                logger.Debug("processing order {0}", order.Id);
                order.Status = OrderStatus.Complete;
                var invite = DbContext.Invitations.FirstOrDefault(i => i.Order.Id == order.Id);
                if (invite != null)
                {
                    invite.Status = InvitationStatus.Accepted;
                    invite.AcceptedOn = DateTime.Now;
                    invite.Student = DbContext.Students.FirstOrDefault(s => s.Id == order.ChildId);
                }
                else
                {
                    logger.Info("Invite is null for order id {0}", order.Id);
                }
                try
                {
                    DbContext.SaveChanges();
                }
                catch (Exception e)
                {
                    logger.ErrorException("Failed to save the order", e);
                    throw;
                }
            }
            else
            {
                logger.Info("Order is null");
            }
        }

        public Event GetEventByOrder(Order order)
        {
            Event eventByOrder = DbContext.Invitations.First(i => i.Order.Id == order.Id).Event;
            return eventByOrder;
        }

    }
}
