using SchoolDuesLib.Models;

namespace SchoolDuesLib.Providers
{
    public class PaymenLogDataAccess:BaseDataAccess
    {
        public PaymenLogDataAccess(string username) : base(username)
        {
        }

        public void LogDwollaResponse (DwollaResponse dwollaResponse)
        {
            DbContext.DwollaResponses.Add(dwollaResponse);
            DbContext.SaveChanges();
        }
    }
}