using System;
using System.Collections.Generic;
using System.Linq;
using SchoolDuesLib.Models;

namespace SchoolDuesLib.Providers
{
    public class ChildDataAccess:BaseDataAccess
    {
        public const string DefaultStudentFirstName = "First";
        public const string DefaultStudentLastName = "Child";

        public ChildDataAccess(string username)
            : base(username)
        {
        }

        public ChildDataAccess(SiteContext dbContext) :base(dbContext)
        {
        }

        public void CreateUpdateChild(Student studentViewModel, string userName)
        {
            var userProfile = DbContext.UserProfiles.FirstOrDefault(u => u.UserName == userName);
            if (userProfile != null)
            {
                // find school, if not exists, create one
                var schoolDataProvider = new SchoolDataProvider(DbContext);

                if (studentViewModel.Id == 0)
                {
                    studentViewModel.Parent1 = userProfile;
                    schoolDataProvider.AddStudent(studentViewModel);
                }
                else
                {
                    Student student = DbContext.Students.First(s => s.Id == studentViewModel.Id);
                    student.DateOfBirth = studentViewModel.DateOfBirth;
                    student.LastName = studentViewModel.LastName;
                    student.FirstName = studentViewModel.FirstName;
                }

                DbContext.SaveChanges();
            }
        }

        public IList<Student> GetStudentsByParentUsername(string userName = null)
        {
            if (userName == null)
            {
                userName = this.Username;
            }
            return DbContext.Students.Include("Class.School")
                .Where(s => s.Parent1.UserName == userName || s.Parent2.UserName == userName).ToList();
        }

        public Student GetStudentsById(int id, string userName)
        {
            return DbContext.Students.Include("Class.School").SingleOrDefault(s=>s.Id == id && 
                (s.Parent1.UserName == userName || s.Parent2.UserName == userName));
        }

        public Student GetDefaultChild(int id)
        {
            var student = DbContext.Students.FirstOrDefault(s => s.Parent1.Id == id || s.Parent2.Id == id);
            
            if (student == null)
            {
                student = new Student
                    {
                        DateOfBirth = DateTime.Today,
                        FirstName = DefaultStudentFirstName,
                        LastName = DefaultStudentLastName
                    };
                student.Parent1 = DbContext.UserProfiles.First(u => u.Id == id);
                DbContext.Students.Add(student);

                DbContext.SaveChanges();
            }

            return student;
        }
    }
}