﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NLog;
using SchoolDuesLib.Models;

namespace SchoolDuesLib.Providers
{
    public class SchoolDataProvider:BaseDataAccess
    {
        readonly TextInfo localTextInfo = new CultureInfo("en-US", false).TextInfo;

        public SchoolDataProvider(string username):base(username)
        {
        }

        internal SchoolDataProvider(SiteContext dbContext):base(dbContext)
        {
            
        }

        private string NormalizeString(string s)
        {
            return localTextInfo.ToTitleCase(s.ToLower());
        }

        private static readonly object _syncRoot = new Object();
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public IEnumerable<School> GetSchoolsByTownZip(string city, string state)
        {
            state = state.ToUpper();
            city = NormalizeString(city);
            var schools = DbContext.Schools.Where(s => s.City == city && s.State == state).ToList();

            if (schools.Any())
            {
                return schools;
            }

            logger.Debug("Requesting schools for {0}, {1}", city, state);
            // synchronize logic that pulls from DOE so we don't accidently double pull data for the same town
            lock(_syncRoot)
            {
                // let's check the DB again in case another thread just pulled the data and exited the lock block
                schools = DbContext.Schools.Where(s => s.City == city && s.State == state).ToList();
                logger.Debug("Number schools in the DB after the lock {0}", schools.Count);

                if (schools.Any())
                {
                    return schools;
                }

                schools = PullFromDepartmentOfEducation(city, state).ToList();
                logger.Debug("Schools from DOE {0}", schools.Count);
                foreach (var school in schools)
                {
                    DbContext.Schools.Add(school);
                }

                DbContext.SaveChanges();
            }
            return schools;
        }


        private IEnumerable<School> PullFromDepartmentOfEducation(string city, string state)
        {
            var xmlDocument = new DepartmentOfEducation().Search(string.Format("{0} {1}", city, state));
            logger.Debug(xmlDocument.ToString);
            var rows = xmlDocument.Descendants("row");
            rows = rows.Where(r => r.Element("lcity09") != null && r.Element("lstate09") != null);

            var xElements =
                rows.Where(d => d.Element("lcity09").Value == city.ToUpper() && d.Element("lstate09").Value == state.ToUpper()).
                    ToList();

            foreach (var xElement in xElements)
            {
                var schoolName = xElement.Element("schnam09");
                var schoolId = xElement.Element("ncessch");
                if (schoolName != null && schoolId != null)
                {
                    var departmentOfEducationId = schoolId.Value;
                    yield return new School
                    {
                        Name = NormalizeString(schoolName.Value.ToLower()),
                        City = NormalizeString(city),
                        State = state.ToUpper(),
                        Address = (xElement.Element("mstree09") != null)?xElement.Element("mstree09").Value:"",
                        ZipCode = (xElement.Element("mzip09") != null) ? xElement.Element("mzip09").Value : "",
                        PhoneNumber = (xElement.Element("phone09") != null) ? xElement.Element("phone09").Value : "",
                        DepartmentOfEducationId = schoolId.Value
                    };

                }
            }
        }

        public School GetSchool(int schoolId)
        {
            return DbContext.Schools.FirstOrDefault(s => s.Id == schoolId);
        }

        public School CreateSchool(string schoolName, string city, string state)
        {
            var school = DbContext.Schools.FirstOrDefault(s => s.Name == schoolName && s.City == city && s.State == state);
            if (school != null)
            {
                return school;
            }
            school = new School
                {
                    Name = schoolName,
                    City = city,
                    State = state
                };
            DbContext.Schools.Add(school);
            DbContext.SaveChanges();
            return school;
        }
        

        public ClassRoom GetClassRoomByIdOrTeachersName(int classRoomId, int schoolId, string teachersName, string grade)
        {
            ClassRoom classRoom;
/*
            classRoom = DbContext.ClassRooms.FirstOrDefault(c => c.Id == classRoomId);
            if (classRoom == null)
*/
            {
                classRoom =
                    DbContext.ClassRooms.FirstOrDefault(
                        c =>
                        c.SchoolId == schoolId &&
                        String.Compare(c.Teacher, teachersName, StringComparison.OrdinalIgnoreCase) == 0 &&
                        c.Grade == grade);
            }
            return classRoom;
        }

        public ClassRoom CreateClassRoom(School school, string teacherName, string grade)
        {
            var classRoom = new ClassRoom
                {
                    School = school, Grade = grade, Teacher = teacherName
                };
            DbContext.ClassRooms.Add(classRoom);
            DbContext.SaveChanges();

            return classRoom;
        }

        public void AddStudent(Student student)
        {
            DbContext.Students.Add(student);
            DbContext.SaveChanges();
        }

        public School GetOrCreateSchool(int schoolId, string name, string city, string state)
        {
            School school = GetSchool(schoolId);
            if (school == null || String.Compare(school.Name, name, StringComparison.OrdinalIgnoreCase) != 0)
            {
                school = CreateSchool(name, city, state);
            }
            return school;

        }

        public List<School> GetUserSchools(string userName)
        {
            var userProfile = DbContext.UserProfiles.FirstOrDefault(u => u.UserName == userName);
            if (userProfile != null)
            {
                var schools =
                    DbContext.Students.Where(s => s.Parent1.Id == userProfile.Id || s.Parent2.Id == userProfile.Id)
                    .Select(s=>s.Class.School).Distinct().ToList();

                schools.AddRange(userProfile.UserSchools.ToList());

                return schools.Distinct().ToList();
            }
            return null;
        }
    }
}
