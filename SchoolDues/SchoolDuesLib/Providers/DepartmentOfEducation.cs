using System;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Xml.Linq;
using NLog;

namespace SchoolDuesLib.Providers
{
    public class DepartmentOfEducation
    {
        private const string url = @"https://explore.data.gov/views/ykv5-fn9t/rows.xml?search={0}";
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public XDocument Search(string searchTerm)
        {
            log.Debug("Search terms = {0}", searchTerm);
            try
            {
                var xmlDocument = XDocument.Load(string.Format(url, HttpUtility.UrlEncode(searchTerm)));
                return xmlDocument;
            }
            catch (Exception e)
            {
                log.ErrorException("Failed to get data from DoE", e);
            }
            return null;
        }
    }
}