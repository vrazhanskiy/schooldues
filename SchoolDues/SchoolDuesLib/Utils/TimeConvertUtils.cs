﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using NLog;
using SchoolDuesLib.Models;

namespace SchoolDuesLib.Utils
{
    public static class TimeConvertUtils
    {
        internal static void UpdateToUserTimezone(this List<Event> ev, string userName)
        {
            ev.ForEach(e=> e.UpdateToUserTimezone(userName));
        }

        internal static void UpdateToUserTimezone(this Event ev, string userName)
        {
            if (ev.StartTime != null)
            {
                ev.StartTime = ev.StartTime.Value.ToUserLocalTime(userName);
            }
            if (ev.EndTime != null)
            {
                ev.EndTime = ev.EndTime.Value.ToUserLocalTime(userName);
            }
            if (ev.VolunteerSpots != null)
            {
                foreach (var volunteerSpot in ev.VolunteerSpots)
                {
                    volunteerSpot.StartTime = volunteerSpot.StartTime.ToUserLocalTime(userName);
                    volunteerSpot.EndTime = volunteerSpot.EndTime.ToUserLocalTime(userName);
                }
            }
        }

        internal static void UpdateToUniversalTime(this List<Event> ev, string userName)
        {
            ev.ForEach(e => e.UpdateToUniversalTime(userName));
        }
        internal static void UpdateToUniversalTime(this Event ev, string userName)
        {
            if (ev.StartTime != null)
            {
                ev.StartTime = ev.StartTime.Value.ToUserUtcTime(userName);
            }
            if (ev.EndTime != null)
            {
                ev.EndTime = ev.EndTime.Value.ToUserUtcTime(userName);
            }
            if (ev.VolunteerSpots != null)
            {
                foreach (var volunteerSpot in ev.VolunteerSpots)
                {
                    volunteerSpot.StartTime = volunteerSpot.StartTime.ToUserUtcTime(userName);
                    volunteerSpot.EndTime = volunteerSpot.EndTime.ToUserUtcTime(userName);
                }
            }
        }

        public static DateTime ToUserLocalTime(this DateTime dt, string userId)
        {
            if (dt.Kind == DateTimeKind.Utc)
            {
                return TimeZoneInfo.ConvertTimeFromUtc(dt, GetUserTimeZone(userId));
            }
            return dt;
        }

        public static DateTime ToUserUtcTime(this DateTime dt, string userId)
        {
            if (dt.Kind ==DateTimeKind.Unspecified)
            {
                return TimeZoneInfo.ConvertTimeToUtc(dt, GetUserTimeZone(userId));
            }
            return dt;
        }

        private static readonly ConcurrentDictionary<string, TimeZoneInfo> userToTimezone =
            new ConcurrentDictionary<string, TimeZoneInfo>();

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private static TimeZoneInfo GetUserTimeZone(string username)
        {
            return userToTimezone.GetOrAdd(username, LookupUserTimezone);
        }

        private static TimeZoneInfo LookupUserTimezone(string username)
        {
            var dbContext = new SiteContext(username);
            var foundUser = dbContext.UserProfiles.FirstOrDefault(u => u.UserName == username);
            if (foundUser != null && foundUser.Timezone != null)
            {
                try
                {
                    return TimeZoneInfo.FindSystemTimeZoneById(foundUser.Timezone.SystemName);
                }
                catch (Exception e)
                {
                    logger.ErrorException("Failed to parse timezone", e);
                }
            }

            return TimeZoneInfo.Local; // return server time zone
        }
    }
}
