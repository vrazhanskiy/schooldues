﻿using System.Web;

namespace SchoolDuesLib.Utils
{
    public static class Helper
    {
        public static string GetBaseUrl(this HttpRequest request)
        {
            return string.Format("{0}://{1}{2}",
                                 request.Url.Scheme,
                                 request.Headers["host"],
                                 request.ApplicationPath.TrimEnd('/'));
        }
    }
}
