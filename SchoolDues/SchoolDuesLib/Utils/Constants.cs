﻿namespace SchoolDuesLib.Utils
{
    public class Constants
    {
        public const string ParentRoleName = "Parent";
        public const string EventAdminRoleName = "EventAdmin";
        public const string SystemAdminRoleName = "SysAdmin";
    }
}