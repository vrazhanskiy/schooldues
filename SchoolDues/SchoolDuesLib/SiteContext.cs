﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using SchoolDuesLib.Models;
using SchoolDuesLib.Utils;

namespace SchoolDuesLib
{
    public class SiteContext:DbContext
    {
        private readonly string userName;
        public DbSet<School> Schools{ get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<ClassRoom> ClassRooms { get; set; }
        public DbSet<VolunteerSpot> VolunteerSpots { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventSupply> EventSupplies { get; set; }
        public DbSet<Invitation> Invitations{ get; set; }
        public DbSet<Order> Orders{ get; set; }

        public DbSet<DwollaResponse> DwollaResponses{ get; set; }
        public DbSet<Log> Logs{ get; set; }

        public DbSet<Timezone> Timezones { get; set; }

        public SiteContext(): base("DefaultConnection")
        {
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectMaterialized;
        }

        public SiteContext(string userName) : base("DefaultConnection")
        {
            this.userName = userName;
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectMaterialized;
        }

        private void ObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
            var ev = e.Entity as Event;
            var spot = e.Entity as VolunteerSpot;
            if (ev != null)
            {
                if (ev.StartTime != null)
                {
                    ev.StartTime = DateTime.SpecifyKind(ev.StartTime.Value, DateTimeKind.Utc);
                }
                if (ev.EndTime != null)
                {
                    ev.EndTime = DateTime.SpecifyKind(ev.EndTime.Value, DateTimeKind.Utc);
                }
            }
            else if (spot != null)
            {
                spot.StartTime = DateTime.SpecifyKind(spot.StartTime, DateTimeKind.Utc);
                spot.EndTime = DateTime.SpecifyKind(spot.EndTime, DateTimeKind.Utc);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProfile>().HasMany(u=>u.Events).WithMany(e=>e.Admins)
              .Map(m =>
              {
                  m.MapLeftKey("UserId");
                  m.MapRightKey("EventId");
                  m.ToTable("UserEventAdmins");
              });
            modelBuilder.Entity<UserProfile>().HasMany(u=>u.VolunteerSpots).WithMany(e=>e.Accepted)
              .Map(m =>
              {
                  m.MapLeftKey("UserId");
                  m.MapRightKey("VolunteerSpotId");
                  m.ToTable("UserVolunteerSpots");
              });

            modelBuilder.Entity<UserPaymentAccount>().HasMany(upa => upa.Events).WithMany(e => e.AcceptablePayments)
                .Map(m =>
                    {
                        m.MapLeftKey("UserPaymentAccountId");
                        m.MapRightKey("EventId");
                        m.ToTable("EventAcceptablePayment");
                    });
        }

        public override int SaveChanges()
        {
            IEnumerable<DbEntityEntry> entries = this.ChangeTracker.Entries();
            foreach (var entry in entries)
            {
                Event ev = entry.Entity as Event;
                var invitation = entry.Entity as Invitation;
                if (ev != null)
                {
                    ev.UpdateToUniversalTime(userName);
                }
            }
            return base.SaveChanges();
        }
    }

    public class DbInitializer:DropCreateDatabaseIfModelChanges<SiteContext>
    {
        protected override void Seed(SiteContext context)
        {
            base.Seed(context);

            context.SaveChanges();
        }
    }
}
